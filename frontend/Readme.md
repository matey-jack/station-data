
# Some related Information

* [https://de.wikipedia.org/wiki/Bahnsteighöhe_(Deutschland)]
* [https://de.wikipedia.org/wiki/Bahnsteighöhe]

## Höhen

[Bahnsteighöhenkonzept Deutsche Bahn](https://www.deutschebahn.com/resource/blob/1173544/52654276c4eebdd3e6d2219423424773/ETR-05_2014-Bahnsteigh%C3%B6henkonzept-data.pdf)
[Bahnsteighöhen Baden-Würtemberg](https://vm.baden-wuerttemberg.de/de/ministerium/presse/pressemitteilung/pid/minister-hermann-barrierefreiheit-ist-das-ziel/)

## Längen

[Konzept NRW](http://www.kcitf-nrw.de/infrastrukturplanung/dokumente/pdf/stationsuebersicht-bahnsteignutzlaengen-nrw.pdf)


# Development

Hacking on this is really easy!

Get the dart-sdk: [https://www.dartlang.org/tools/sdk]
 - there's a Debian repository for Ubuntu & friends.
 - choco for Windows and brew for Mac

Do this to get your web tools set up (same command for installs and 
updates which are also necessary when some of your deps are updated...):

    pub global activate webdev
    
And then run the app locally:

    webdev serve
    
And then run tests:

    pub test
    
And then minify and bundle to get a fast & small JavaScript app:

    webdev build
    
And when your state becomes messed and gives weird errors, you can try:

    pub run build_runner clean
    
And this one only works for me (unless you change web/CNAME to another domain of your choice):
    
    surge ./build
    
And the beta-version:

    surge ./build https://station-data-beta.surge.sh


