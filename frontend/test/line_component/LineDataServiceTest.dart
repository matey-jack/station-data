import 'package:test/test.dart';
import 'package:station_data/line_component/LineDataService.dart';

void main() {
  group('Route', () {
      test('stopsAt 12', () {
          final stop = Stop("lala", "123", 12);

          expect(stop.stopsAtRoute(0), equals(false));
          expect(stop.stopsAtRoute(1), equals(false));
          expect(stop.stopsAtRoute(2), equals(true));
          expect(stop.stopsAtRoute(3), equals(true));
      });

      test('stopsAt 3', () {
          final stop = Stop("lala", "123", 3);

          expect(stop.stopsAtRoute(0), equals(true));
          expect(stop.stopsAtRoute(1), equals(true));
          expect(stop.stopsAtRoute(2), equals(false));
          expect(stop.stopsAtRoute(3), equals(false));
      });
  });
}