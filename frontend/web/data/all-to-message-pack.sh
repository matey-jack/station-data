# wget https://github.com/ludocode/msgpack-tools/releases/download/v0.6/msgpack-tools-0.6-x86_64.deb
# dpkg -i tools-0.6-x86_64.deb

for f in *.json 
do 
    json2msgpack -i $f -o ${f%.json}.msgp 
done

# Result: we save less 12% to 25% on spaceless json and less than 50% on 
# prettified json with nesting, even less when it's not nested much.
# That would be a big impact if we'd have to pay for transport times a million
# users, but in our case it wouldn't change that much.
# Definitely not worth the trouble now!

# Some additional data points on msgpack:
#  - binary data can be faster to parse, but nothing so far beats Browser-native
#    JSON.parse() function

# https://news.ycombinator.com/item?id=4090831
# https://gist.github.com/frsyuki/2908191

# It seems like the old rule "binary data is always much faster" is simply not true
# any more when it comes to Browsers and JSON. Binary is probably still faster
# when big data on servers is concerned or for IPC, but for frontend web development
# I'm pretty sure we can forget about anything but JSON for the coming years or more.
# I remember looking at GMail and Google Docs with its millions of calls to the backend
# and they, too, use just JSON for communicating.

# Also note that all major Browsers and HTTP servers automatically and transparently 
# use compression – so as long as our JSON structure isn't extra wasteful, we're
# already automatically using the most efficient solution!
