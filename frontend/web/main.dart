import 'package:angular/angular.dart';
import 'package:http/http.dart';
import 'package:http/browser_client.dart';
import 'package:angular_router/angular_router.dart';

import 'package:station_data/AppComponent.template.dart' as ng;
import 'package:station_data/line_component/LineDataService.dart';
import 'package:station_data/station_component/StationDataService.dart';

import 'main.template.dart' as self;

@GenerateInjector([
  ClassProvider(Client, useClass: BrowserClient),
  routerProvidersHash, // You can use routerProviders in production
  ClassProvider(StationDataService),
  ClassProvider(LineDataService),
])
final InjectorFactory injector = self.injector$Injector;

void main() {
  runApp(ng.AppComponentNgFactory, createInjector: injector);
}
