import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:http/http.dart';
import 'package:angular/core.dart';

class Network {
  // TODO: we could do it like the StationDataService and only keep jsonData as
  // values in the array and then create the proper classes only on request...
  final Map<String, Line> _linesByName;

  Network(this._linesByName);

  factory Network.fromJson(Map<String, dynamic> json) {
    return Network((json["linesById"] as Map)
        .map((name, data) => MapEntry(name as String, Line.fromJson(data))));
  }

  Line getLine(String name) => _linesByName[name];
}

class Line {
  final String name;
  final List<Route> routes;
  final List<Stop> stops;

  Line(this.name, this.routes, this.stops);

  factory Line.fromJson(json) => Line(
      json["name"] as String,
      (json["routes"] as List).map(Route.fromJson).toList(),
      (json["mergedRoute"] as List)
          .map((json) => Stop.fromJson(json))
          .toList());
}

class Route {
  final int count;
  final String description;

  Route(this.count, this.description);

  static Route fromJson(json) =>
      Route(json["count"] as int, json["stationString"] as String);
}

class Stop {
  final String name;
  final String evaId;
  final int _stopsAtRoutes;

  Stop(this.name, this.evaId, this._stopsAtRoutes);

  factory Stop.fromJson(json) {
    final station = json["station"] as Map<String, dynamic>;
    return Stop(station["name"] as String, station["id"] as String,
        json["stoppingRoutes31"] as int);
  }

  bool stopsAtRoute(int route) => (_stopsAtRoutes >> route) & 1 == 1;
}

@Injectable()
class LineDataService {
  static const _routesUrl = 'data/allRoutes.json';

  final Client _http;

  LineDataService(this._http);

  // indexed by network name
  Map<String, Network> _networks = {};

  Future<void> loadData() async {
    final response = await _http.get(_routesUrl);
    final body = json.decode(utf8.decode(response.bodyBytes)) as Map;
    _networks = (body["linesByNetwork"] as Map)
        .map((name, data) => MapEntry(name as String, Network.fromJson(data)));
  }

  Exception _handleError(dynamic e) {
    return Exception('Server error; cause: $e');
  }

  Line getLine(String network, String lineName) =>
      _networks[network].getLine(lineName);
}
