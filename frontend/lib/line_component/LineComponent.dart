import 'dart:math' as math;

import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

import '../RoutePaths.dart';
import 'LineDataService.dart';
import '../station_component/StationDataService.dart';

@Component(
  selector: 'line-component',
  templateUrl: 'line_component.html',
  styleUrls: ['line_component.css'],
  directives: [coreDirectives, routerDirectives],
)
class LineComponent implements OnInit, OnActivate {
  final LineDataService _lineService;
  final StationDataService _stationService;

  String network;
  String lineName;
  Line line;

  LineComponent(this._lineService, this._stationService);

  @override
  Future<void> ngOnInit() async {
    await _lineService.loadData();
    // clumsy check to see if we're already waiting for the data
    if (network != null) {
      line = _lineService.getLine(network, lineName);
    }
  }

  @override
  void onActivate(RouterState previous, RouterState current) async {
    network = getNetwork(current.parameters);
    lineName = getLine(current.parameters);
    // clumsy way to check that [_service] is ready to serve data
    if (line != null) {
      line = _lineService.getLine(network, lineName);
    }
  }

  String stationUrlForId(Stop stop) {
    final name = _stationService.getStationNameByEvaId(stop.evaId);
    return name == null ? null : stationUrl(name);
  }

  List<T> trimList<T>(List<T> list, int n) =>
      list.sublist(0, math.min(list.length, n));

  List<Route> get someRoutes => trimList(line.routes, 8);

  String platformSummary(Stop stop) {
    List<Platform> platforms = _stationService.getEvaPlatformsById(stop.evaId);
    if (platforms == null || platforms.length == 0) {
      return "---";
    }

    final number = (platforms.length == 1) ? "1 p" : "${platforms.length} p's";

    return "${number}, "
        "${_pfHeight(platforms, "..")} cm, "
        "${_pfLength(platforms, "..")} m.";
  }

  String platformSummaryPhrase(Stop stop) {
    List<Platform> platforms = _stationService.getEvaPlatformsById(stop.evaId);
    if (platforms == null || platforms.length == 0) {
      return "No platform data available.";
    }

    final number = (platforms.length == 1)
        ? "One platform"
        : "${platforms.length} platforms";

    return "${number} of height ${_pfHeight(platforms, " to ")} cm "
        "and length ${_pfLength(platforms, " to ")} m.";
  }

  String _pfHeight(List<Platform> platforms, String connector) {
    final heights = platforms.map((p) => p.height);
    final height = _intervalOrValue(
        heights.reduce(math.min), heights.reduce(math.max), connector);

    return height;
  }

  String _pfLength(List<Platform> platforms, String connector) {
    final lengths = platforms.map((p) => p.length);
    final length = _intervalOrValue(
        lengths.reduce(math.min), lengths.reduce(math.max), connector);
    return length;
  }

  String _intervalOrValue(min, max, String connector) =>
      min == max ? "${min}" : "${min}${connector}${max}";
}
