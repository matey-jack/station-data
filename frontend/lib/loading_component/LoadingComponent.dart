import 'package:angular/angular.dart';
import 'package:angular_router/angular_router.dart';

@Component(
  selector: 'loading-component',
  templateUrl: 'loading_component.html',
  styleUrls: ['loading_component.css'],
  directives: [coreDirectives, routerDirectives],
)
class LoadingComponent {
}
