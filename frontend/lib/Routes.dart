import 'package:angular_router/angular_router.dart';

import 'station_component/StationComponent.template.dart' as stations_template;
import 'line_component/LineComponent.template.dart' as lines_template;
import 'RoutePaths.dart';

export 'RoutePaths.dart';

class Routes {
  static final stations = RouteDefinition(
    routePath: RoutePaths.stations,
    component: stations_template.StationComponentNgFactory,
    useAsDefault: true,
  );

  static final lines = RouteDefinition(
    routePath: RoutePaths.lines,
    component: lines_template.LineComponentNgFactory,
  );

  static final all = <RouteDefinition>[stations, lines];
}
