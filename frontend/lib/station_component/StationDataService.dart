import 'dart:async';
import 'dart:convert';

import 'package:angular/core.dart';
import 'package:http/http.dart';

class EvaStation {
  final String evaId;
  final List<String> ds100;
  final String name;
  LinesByNetwork lines;

  EvaStation(this.evaId, this.ds100, this.name);

  factory EvaStation.fromJson(Map<String, dynamic> json) => EvaStation(
      json['evaId'],
      (json['ds100'] as List).map((el) => el as String).toList(),
      json['name']);

  String get ds100Str => ds100.join(', ');
}

class Platform {
  final String trackName;
  final String platformName;
  final int length;
  final int height;

  Platform(this.trackName, this.platformName, this.length, this.height);

  factory Platform.fromJson(Map<String, dynamic> json) => Platform(
      json['trackName'], json['platformName'], json['length'], json['height']);
}

class InfraStation {
  final String name;
  final String bfNummer;
  final String ds100;
  final List<EvaStation> evaStations;
  final List<Platform> platforms;

  InfraStation(
      this.name, this.bfNummer, this.ds100, this.evaStations, this.platforms);

  factory InfraStation.fromJson(Map<String, dynamic> json) {
    return InfraStation(
      json['name'],
      json['bfNummer'],
      json['ds100'],
      (json['evaStations'] as List)
          .map((eva) => EvaStation.fromJson(eva as Map))
          .toList(),
      (json['platforms'] as List)
          .map((platform) => Platform.fromJson((platform as Map)))
          .toList(),
    );
  }

  EvaStation evaStationById(String evaId) =>
      evaStations.firstWhere((station) => station.evaId == evaId);
}

class LinesByNetwork {
  final Map<String, List<String>> _linesByNetwork;

  LinesByNetwork(this._linesByNetwork);

  factory LinesByNetwork.fromJson(Map<String, dynamic> json) {
    var rawMap = json["linesByNetwork"] as Map;
    return LinesByNetwork(rawMap.map(refsMap));
  }

  static MapEntry<String, List<String>> refsMap(network, lines) =>
      MapEntry(network as String, linesFromJson(lines));

  static List<String> linesFromJson(routeList) => (routeList as List)
      .map<String>((lineName) => lineName as String)
      .toList();

  Iterable<String> get lineNetworks => _linesByNetwork.keys;

  List<String> linesForNetwork(String network) => _linesByNetwork[network];
}

@Injectable()
class StationDataService {
  static const _stationNamesUrl = 'data/stationNames.json';
  static const _allStationsUrl = 'data/allStations.json';
  static const _stationsToRoutesUrl = 'data/stationsToRoutes.json';

  final Client _http;

  StationDataService(this._http);

  List<String> _stationNames = <String>[];

  // indexed by stationName
  Map<String, dynamic> _allStations = {};

  // indexed by evaId
  Map<String, dynamic> _multistationParts = {};

  // indexed by evaId
  Map<String, dynamic> _linesByStation = {};

  Map<String, String> _evaIdToStationName = {};

  Future<void> loadData() =>
      Future.wait([loadStations(), loadStationToRoutes(), getStationNames()]);

  Future<List<String>> getStationNames() async {
    if (_stationNames.isEmpty) {
      try {
        final response = await _http.get(_stationNamesUrl);
        _stationNames = _extractNames(response);
      } catch (e) {
        throw _handleError(e);
      }
    }
    return _stationNames;
  }

  List<String> _extractNames(Response resp) =>
      (json.decode(utf8.decode(resp.bodyBytes))['stationNames'] as List)
          .map((name) => name as String)
          .toList();

  /// getStation() will return [null] until this future returns.
  Future<void> loadStations() async {
    if (_allStations.isNotEmpty) return;
    final stationToRoutesFuture = loadStationToRoutes();

    try {
      final response = await _http.get(_allStationsUrl);
      final body = json.decode(utf8.decode(response.bodyBytes));
      _allStations = body['stations'] as Map;
      _multistationParts = body['multistationParts'] as Map;
      _evaIdToStationName = (body['evaIdToStationName'] as Map)
          .map((id, name) => MapEntry(id as String, name as String));
    } catch (e) {
      throw _handleError(e);
    }
    await stationToRoutesFuture;
  }

  InfraStation getStation(String name) {
    // [name] is one of the station names (infra or eva), but we don't know
    // which one. Don't store it for now.
    final json = _allStations[name];
    if (json == null) return null;
    var station = InfraStation.fromJson(json);
    try {
      station.evaStations.forEach(
          (evaStation) => evaStation.lines = getLines(evaStation.evaId));
    } catch (e) {
      print("routeRefs still not parsing fine...");
      throw e;
    }
    return station;
  }

  /// getStationToRoute() will return [null] until this future returns.
  Future<void> loadStationToRoutes() async {
    if (_linesByStation.isNotEmpty) return;

    try {
      final response = await _http.get(_stationsToRoutesUrl);
      final body = json.decode(utf8.decode(response.bodyBytes));
      _linesByStation = body['linesByStation'] as Map;
    } catch (e) {
      throw _handleError(e);
    }
  }

  LinesByNetwork getLines(String evaId) {
    if (_linesByStation == null) return null;

    final json = _linesByStation[evaId];
    if (json == null) return null;
    return LinesByNetwork.fromJson(json);
  }

  List<Platform> getMultistationPart(EvaStation evaStation) {
    final rawParts = _multistationParts[evaStation.evaId] as List;
    if (rawParts == null) return null;

    return rawParts.map((json) => Platform.fromJson(json)).toList();
  }

  List<Platform> getEvaPlatforms(
      EvaStation evaStation, InfraStation infraStation) {
    final parts = getMultistationPart(evaStation);
    if (parts != null) return parts;

    return infraStation.platforms;
  }

  List<Platform> getEvaPlatformsById(String evaId) {
    final name = _evaIdToStationName[evaId];
    final infraStation = getStation(name);
    if (infraStation == null) return null;
    final evaStation = infraStation.evaStationById(evaId);
    return getEvaPlatforms(evaStation, infraStation);
  }

  String getStationNameByEvaId(String id) =>
      _evaIdToStationName == null ? null : _evaIdToStationName[id];

  Exception _handleError(dynamic e) {
    print(e); // for demo purposes only
    return Exception('Server error; cause: $e');
  }
}
