import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';

import '../Routes.dart';
import '../RoutePaths.dart';
import 'StationDataService.dart';

@Component(
    selector: 'station-component',
    templateUrl: 'station_component.html',
    styleUrls: [
      'station_component.css'
    ],
    directives: [
      coreDirectives,
      MaterialAutoSuggestInputComponent,
      routerDirectives
    ],
    providers: [
      materialProviders,
    ],
    exports: [
      Routes,
      RoutePaths,
      lineUrl
    ])
class StationComponent implements OnInit, OnActivate {
  final StationDataService _service;
  final Router _router;

  bool stationsLoaded = false;
  List<String> stationNames = [];
  String stationName;

  InfraStation station;

  bool get noStationSelected =>
      !stationsLoaded || !stationNames.contains(stationName) && station == null;

  bool get noStationFound =>
      stationsLoaded && stationNames.contains(stationName) && station == null;

  StationComponent(this._service, this._router);

  void ngOnInit() async {
    stationNames = await _service.getStationNames();
    await _service.loadStations();
    stationsLoaded = true;
    if (stationName != null) {
      station = _service.getStation(stationName);
    }
  }

  @override
  void onActivate(_, RouterState current) async {
    stationName = getStation(current.parameters);
    if (stationName != null) {
      setStation(stationName);
    }
  }

  void onSelectStation(String name) {
    setStation(name);
    if (station != null) {
      _router.navigate(stationUrl(name));
    }
  }

  void setStation(String name) {
    if (!stationsLoaded) return;

    station = _service.getStation(name) ?? station;
  }

  List<Platform> getPlatforms(evaStation) =>
      _service.getEvaPlatforms(evaStation, station);

  // didn't find ngPlural pipe, so do it by hand.
  String get numEvaStationsPluralized {
    final num = station.evaStations.length;
    return (num == 0)
        ? "no stations"
        : (num == 1) ? "one station" : "$num stations";
  }
}
