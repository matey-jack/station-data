import 'package:angular_router/angular_router.dart';

const stationParam = 'station';
const networkParam = 'network';
const lineParam = 'line';

const _space_replacement = '_';

class RoutePaths {
  static final stations = RoutePath(path: 'stations/:$stationParam');
  static final lines = RoutePath(path: 'lines/:$networkParam/:$lineParam');
}

String lineUrl(String network, String line) => RoutePaths.lines
    .toUrl(parameters: {networkParam: network, lineParam: line});

String stationUrl(String name) => RoutePaths.stations.toUrl(
    parameters: {stationParam: name.replaceAll(' ', _space_replacement)});

String getStation(Map<String, String> parameters) =>
    parameters[stationParam]?.replaceAll(_space_replacement, ' ');

String getNetwork(Map<String, String> parameters) => parameters[networkParam];

String getLine(Map<String, String> parameters) => parameters[lineParam];
