import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:angular_router/angular_router.dart';

import 'Routes.dart';
import 'line_component/LineDataService.dart';
import 'station_component/StationDataService.dart';
import 'loading_component/LoadingComponent.dart';

@Component(
  selector: 'my-app',
  templateUrl: 'app_component.html',
  styleUrls: ['app_component.css'],
  directives: [coreDirectives, routerDirectives, LoadingComponent],
  providers: [materialProviders],
  exports: [RoutePaths, Routes],
)
class AppComponent implements OnInit {
  final LineDataService _lineService;
  final StationDataService _stationService;

  var loading = true;

  AppComponent(this._lineService, this._stationService);

  void ngOnInit() async {
    await Future.wait([
      _lineService.loadData(),
      _stationService.loadData(),
      // let's make loading take some minimum time so that the colored image
      // doesn't disappear too quickly.
      Future.delayed(Duration(seconds: 2))
    ]);
    loading = false;
  }
}
