What is a DevLog? – It's a Backlog and Changelog in one piece!
--------------------------------------------------------------

Since I don't use versions and releases until I have users, I will instead use the convention
of keeping a feature in "in progress" until its details are working and then add an entry to 
the "done" section.
Things at the top of the backlog and in-progress are usually much more detailed than the bottom
of the backlog and the "done" section, but keeping all in the same file helped me in my last
project, so I will do it, here, too.

Done:
 - reading all needed GTFS data
 - reading all needed station data
 - getting track information from GTFS stop_times.csv
 - writing condensed files stations.json and routes.json 
   which can be displayed by a client-only frontend app
 - multi-stations platforms are mapped to eva-stations

 - station name is now an anchor in URL so that deep links can be sent around
   (manually encodes space with '_' for better readability)

 - backend writes multi-network route information, including routes stopping at each station

 - showing all routes on a line in a merged view with stopping patterns of different services 
   on a single line.

 - frontend loads all data files ahead of time, so we never need to check for data availability
   while the program is running (plus: nice loading screen)

nice to have (do while waiting for answers to my publicity emails) 
 - sticky header in line view
 - redirection RB00 --> RB00a (if RB00 doesn't exist)
 - possibly list of similarly named routes as small links at top or bottom
 - static link from lines to stations view?
 - work around MDV missing directions
 - basic automated regression tests
 
todo after milestone
 - add Verbund aka Provider aka Network logos to lines
 - S-Bahn station evaIds
 - minimal station-number from GTFS fails with multistations – make it more clever!
 - bug: Jungfernheide hat S- und Fernbahnsteige verwechselt
 - debug route merging
 - MDV: count irregular days for ServicePattern (because there are no regular patterns)
 - add travel time to routes
    
 - /frontend/ Page title dependent on ngRoute including the station or train-route that is
   currently shown! Would make Browser history much more useful! 
   https://ilikerobots.github.io/dart/angular2/software/2016/11/25/setting-document-title-in-angular2-dart-improved.html

 - clean up the lazy loading code to make it conform to some best practices.
   Maybe this should start with the decision whether I want to display a "Loading..." thingy
   until all of the data is loaded or whether there should be several possible stages. 
   (Currently we can select a station before all stations are loaded and can look at stations
   before lines are loaded. Actually it would be good to always have minimal requirements because
   that also creates some robustness in case one of the inputs can't be loaded.)
   
Backlog:
 - more aesthetic route merging which groups branches of a line together (instead of having blocks with
   random interleaving of stations that have no relative order). Maybe this could be quite simple. ;-)
   
 - /frontend/ graphically show length of platform (start with <div style="width: {{length}}px"></div>)
 
 - find some way to get route information with typical tracks a route is using. 
   This allows us to show only those tracks in the route view which makes it so much better at bigger stations.
 
 - better CSS
 
 - potentially pair up tracks (platform edges) which are on the same platform.
   (to support this: statistics on any platforms with more than 2 tracks on them)

 - include eva-only stations in front-end (makes sense once we have routes for such stations,
   but don't forget those are very few!)

 - instead of (or in addition to) storing all the transit feeds in the code repository,
   have some scripts which download them from here: https://navitia.opendatasoft.com/explore/dataset/de/table/?sort=type_file
   --> Nuremberg is an especially interesting one (if it includes the S-Bahn), 
       because like Bremen, building of 76 cm platforms is still in progress and we can track it

 - download scripts for station data (which might also get updated from time to time...)

 
Tech Strategy:
 - /frontend/ try this at some time: https://news.dartlang.org/2017/03/making-dart-web-app-offline-capable-3.html 
 - /backend/ Switch to JRE 11 when Amazon Corretto is available in this version, because that will be a good
   stable base moving forward. I could, of course stay with JRE 8 for a while longer, but it's good to be ready
   for the first J11-only dependency or a sudden stop of security updates for J8.
 - /data model/ having a map in the json from backend makes lookups easy, but also means that the keys are duplicate
   unless we omit them in the file and let the service in frontend put them back into the model objects
   from the current key...
   