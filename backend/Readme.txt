Shows platform lengths for all stations on a given line (actually a given train journey on that line,
see below).

Input: a journey id which you can get from the departure board

Used Data:
 - VBB Fahrplan: https://www.vbb.de/unsere-themen/vbbdigital/api-entwicklerinfos/datensaetze
      or https://navitia.opendatasoft.com/explore/dataset/de/table/?sort=type_file

 - MDS Fahrplandaten (Halle-Leipzig / Mitteldeutschland) muss mensch per Email anfordern:
   --> siehe https://www.mdv.de/service/downloads/

 - NEW(2024): GTFS for all of Germany from https://www.opendata-oepnv.de/index.php?id=1384&tx_vrrkit_view%5Bsharing%5D=eyJkYXRhc2V0IjoiZGV1dHNjaGxhbmR3ZWl0ZS1zb2xsZmFocnBsYW5kYXRlbi1ndGZzIiwidXNlcklkIjozNDk1fQ%3D%3D&tx_vrrkit_view%5Baction%5D=download&tx_vrrkit_view%5Bcontroller%5D=View

 - station data from DB Open Data platform
     - http://data.deutschebahn.com/organization/db-station-service
     - copies of files checked in on 2018-Oct
     - two different lists of stations (see below)
     - list of platforms for all stations

not used data:
 - https://developer.deutschebahn.com/store/apis/info?name=Fahrplan&version=v1&provider=DBOpenData#!/default/get_journeyDetails_id
 --> this has only Fernverkehr,
 - public-transit-enabler (Öffi's backend)

plan for filtering exceptional services:
 - see serviceId in trips.csv
 - all those which have no regular running days
 - short validity period (end minus start date)

Fremdschlüssel:
 - Fahrplan-API and GTFS data has EVA-Nr. (8000001 to 8079607)
 - "D_Bahnhof" has EVA-Nr., DS100, Name (plus coordinates and Regio/FV Flags)
    - DS100 letter code where the first letter is one of 15 regional centers
    - ca. 1300 additional entries for which we do not have platform data (possibly NE-Bahnen, non-federal railways)
    - see http://www.bahnseite.de/DS100/DS100_main.html
    - side project: would be nice to make a map of this
 - "Übersicht Bahnhöfe" has DS100, Name (possibly different spelling) and Bf.Nr (integer without apparent structure)
 - "Bahnsteigdaten" has only Bf.Nr
