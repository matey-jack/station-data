  //************************//
 //   Licence information  //
//************************//
this database is provided by Verkehrsverbund Berlin-Brandenburg (VBB) for more information, please visit http://daten.berlin.de
Licence: CC Namensnennung
Original licence or EULA
http://creativecommons.org/licenses/by/3.0/de/


Der Verkehrsverbund Berlin-Brandenburg (VBB) stellt hiermit Bus- und Bahn-Fahrplandaten aus Berlin und Brandenburg im
GTFS-Format zur Verfügung und enthält Linien, Abfahrtzeiten, Routen usw.
(mehr zu dem Format hier https://developers.google.com/transit/gtfs/ (auf Englisch)).
Zweck der Veröffentlichung der Daten sind weitere Verwendungen zu Kundeninformationszwecken zu generieren um neue Nutzer
für den öffentlichen Nahverkehr zu gewinnen. Das Datenset besitzt die Gültigkeit Februar bis Dezember 2016.

Für die erforderliche Namensnennung ist „VBB Verkehrsverbund Berlin-Brandenburg GmbH“ zu verwenden
(VBB-Logo gibt es hier unter http://www.vbb.de/de/article/media-service/logos/2660.html).
Des Weiteren stehen für eine einheitliche Kundenkommunikation die VBB-Verkehrsmittellogos unter
http://www.vbb.de/de/article/media-service/produktsignets/3306.html zur Verfügung.
