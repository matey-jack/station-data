package routes

import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions

class RouteMergingTest {
    /*
    VBB-Routes which still merge incorrectly and could be turned into test data:
    RE3, RB51, RB93, S4 (Wurzen–Leipzig–Eilenburg... und Hoyerswerda im VBB-Land)
     */
    val stations = (0..30)
        .map(Int::toString)
        .map { GtfsStation(it, it) }

    fun mkRoute(stops: List<Int>) =
        Route(stops.map(stations::get), 0)

    @Test
    fun ambiguousStart() {
        val routes = listOf(
            mkRoute(listOf(0, 1, 2, 3, 15, 20)),
            mkRoute(listOf(7, 8, 9, 19, 20, 21))
        )
        val merged = mergeRoutes(routes)
        println(merged.joinToString { it.station.name })

        val mergedIds = merged.map { it.station.id }
        routes.forEach { route ->
            Assertions.assertThat(mergedIds).containsSubsequence(route.stops.map { it.id })
        }
    }

    @Test
    fun ambiguousMiddle() {
        val routes = listOf(
            mkRoute(listOf(0, 1, 2, 3)),
            mkRoute(listOf(0, 3, 4, 7, 8, 22)),
            mkRoute(listOf(12, 13, 15, 22, 23)),
            mkRoute(listOf(0, 1, 2, 3, 4, 5, 10, 11, 12, 13, 15, 22, 23))
        )
        val merged = mergeRoutes(routes)
        println(merged.joinToString { it.station.name })

        val mergedIds = merged.map { it.station.id }
        routes.forEach { route ->
            Assertions.assertThat(mergedIds).containsSubsequence(route.stops.map { it.id })
        }
    }

    @Test
    fun ambiguousEndWithAdditionalExpressTrips() {
        val routes = listOf(
            mkRoute(listOf(0, 11, 12, 17, 22, 23, 26, 29, 30)),
            mkRoute(listOf(0, 11, 21, 22, 23, 26, 29, 30)),
            mkRoute(listOf(0, 1, 2, 3, 4, 7, 8, 11, 12, 16, 22, 24, 25, 27)),
            mkRoute(listOf(0, 1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 16, 22, 24, 25, 27))
        )
        val merged = mergeRoutes(routes)
        println(merged.joinToString { it.station.name })

        val mergedIds = merged.map { it.station.id }
        routes.forEach { route ->
            Assertions.assertThat(mergedIds).containsSubsequence(route.stops.map { it.id })
        }
    }

    @Test
    fun minimalInconsistent() {
        val routes = listOf(
            mkRoute(listOf(0, 1)),
            mkRoute(listOf(1, 0))
        )
        val merged = mergeRoutes(routes)
        println(merged.joinToString { it.station.name })
        Assertions.assertThat(merged).isEmpty()
    }
}