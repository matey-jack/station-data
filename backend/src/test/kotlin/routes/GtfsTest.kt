package routes

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS

@TestInstance(PER_CLASS)
class GtfsVbbTest {
    lateinit var dataSet: GtfsDataSet

    @BeforeAll
    fun loadData() {
        dataSet = GtfsDataSet(sources.first())
        dataSet.readAllTheThings()
    }

    @Test
    fun checkCounts() {
        Assertions.assertThat(dataSet.servicesById.count()).isEqualTo(1779)
        Assertions.assertThat(dataSet.stations.count()).isEqualTo(41529)
        Assertions.assertThat(dataSet.tripsById.count()).isEqualTo(14578)
        Assertions.assertThat(dataSet.linesById.count()).isEqualTo(96)
        val numRoutes = dataSet.linesById.values
            .map { it.routes.count() }
            .sum()
        Assertions.assertThat(numRoutes).isEqualTo(824)
    }

    @Test
    fun checkExamples() {
        Assertions.assertThat(dataSet.linesById["RB7"]!!.routes.first().stationString)
            .isEqualTo("Wustermark <–> Berlin-Jungfernheide (5 times per week, with 6 stops)")

        val re2b = dataSet.linesById["RE2b"]!!
        Assertions.assertThat(re2b.routes[0].stationString)
            .isEqualTo("Nauen <–> Berlin-Südkreuz (10 times per week, with 11 stops)")
        Assertions.assertThat(re2b.routes[2].stationString)
            .isEqualTo("Nauen <–> Berlin-Gesundbrunnen (5 times per week, with 8 stops)")

        val s85 = dataSet.linesById["S85"]!!
        Assertions.assertThat(s85.routes[0].stationString)
            .isEqualTo("Pankow (Berlin) <–> Grünau (Berlin) (424 times per week, with 16 stops)")
        Assertions.assertThat(s85.routes[1].stationString)
            .isEqualTo("Pankow (Berlin) <–> Berlin-Schöneweide (218 times per week, with 13 stops)")

    }
}