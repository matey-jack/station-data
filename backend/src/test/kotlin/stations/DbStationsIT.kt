package stations

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle

@TestInstance(Lifecycle.PER_CLASS)
internal class DbStationsIT {
    private val stations = DbStations().apply {
        readAllTheThings()
    }

    @Test
    fun evaStationCounts() {
        // use a unique instance here, because full loading overwrites 'evaByName'
        val evaOnlyStations = DbStations()
        evaOnlyStations.readEvaStations()
        assertThat(evaOnlyStations.evaByName).hasSize(6605) // that's exactly the file size
        assertThat(evaOnlyStations.evaByDs100).hasSize(6720) // includes a few d

    }
    @Test
    fun stationCounts() {
        assertThat(stations.stationsByBfNr).hasSize(5367) // that's exactly the file size

        // thanks to multi-stations, we have more, despite not matching some others!
        assertThat(stations.stationsByEvaId).hasSize(5371)

        // multistations bring extra DS100 ids
        // (some even more than one, but some none, if the entire station just has one)
        assertThat(stations.stationsByDs100).hasSize(5570)
    }

    @Test
    fun platformCounts() {
        assertThat(stations.stationsByBfNr.values.map { it.platforms.size }.sum())
            .isEqualTo(11901)
    }

    @Test
    fun multistationCounts() {
        // check a few import ones
        assertThat(stations.multistations.map { it.name })
            .contains("Berlin Hauptbahnhof", "Hamburg Hbf", "München Hbf", "Köln Messe/Deutz",
                    "Stuttgart Hbf", "Frankfurt (Main) Hbf", "Leipzig Hbf")

        // check some tricky ones, that we had missed before
        assertThat(stations.multistations.map { it.name })
            .contains("Berlin-Gesundbrunnen", "Berlin-Schöneweide", "Ostkreuz")

        // finally just count the rest
        assertThat(stations.multistations.size).isEqualTo(21)
    }

    @Test
    fun multistationParts() {
        val parts = stations.getMultistationParts()

        // hard-coded special cases first
        val Leipzig_Hbf_tief = parts["8098205"]!!
        assertThat(Leipzig_Hbf_tief).hasSize(2)
        assertThat(Leipzig_Hbf_tief.map { it.trackName })
            .contains("Gleis 1", "Gleis 2")

        val Berlin_Hbf_tief = parts["8098160"]!!
        assertThat(Berlin_Hbf_tief).hasSize(8)
        assertThat(Berlin_Hbf_tief.map { it.trackName })
            .contains("Gleis 1", "Gleis 2", /* ... */ "Gleis 7", "Gleis 8")

        val Berlin_Hbf_S_Bahn = parts["8089021"]!!
        assertThat(Berlin_Hbf_S_Bahn).hasSize(2)
        assertThat(Berlin_Hbf_S_Bahn.map { it.trackName })
            .contains("Gleis 15", "Gleis 16")

        val Berlin_Hbf_Stadtbahn = parts["8011160"]!!
        assertThat(Berlin_Hbf_Stadtbahn).hasSize(4)
        assertThat(Berlin_Hbf_Stadtbahn.map { it.trackName })
            .contains("Gleis 11", "Gleis 12", "Gleis 13", "Gleis 14")

        // then a simple S-Bahn case
        val S_Hbf_S_Bahn = parts["8098096"]!!
        assertThat(S_Hbf_S_Bahn).hasSize(2)
        assertThat(S_Hbf_S_Bahn.map { it.trackName })
            .contains("Gleis 101", "Gleis 102")

        val S_Hbf = parts["8000096"]!!
        assertThat(S_Hbf).hasSize(16)

        // special S-Bahn station, without special rules
        val Berlin_Ostkreuz = parts["8011162"]!!
        assertThat(Berlin_Ostkreuz).hasSize(6)

        val Berlin_Ostkreuz_S_Bahn = parts["8089028"]!!
        assertThat(Berlin_Ostkreuz_S_Bahn).hasSize(6)

        // and the "Gl." cases
        val M_Hbf_S_Bahn = parts["8098263"]!!
        assertThat(M_Hbf_S_Bahn).hasSize(4)
        assertThat(M_Hbf_S_Bahn.map { it.trackName })
            .contains("Gleis 1", "Gleis 1a", "Gleis 2", "Gleis 2a")

        val M_Hbf_Holzkirchener = parts["8098262"]!!
        assertThat(M_Hbf_Holzkirchener).hasSize(7)
        assertThat(M_Hbf_Holzkirchener.map { it.trackName })
            .contains("Gleis 5", "Gleis 6", /* ... */ "Gleis 10", "Gleis 10a")

        val M_Hbf_Starnberger = parts["8098261"]!!
        assertThat(M_Hbf_Starnberger).hasSize(11)
        assertThat(M_Hbf_Starnberger.map { it.trackName })
            .contains("Gleis 27", "Gleis 27a", /* ... */ "Gleis 35", "Gleis 36")

        val M_Hbf_Rest = parts["8000261"]!!
        assertThat(M_Hbf_Rest).hasSize(16)
        assertThat(M_Hbf_Rest.map { it.trackName })
            .contains("Gleis 11", "Gleis 12", /* ... */ "Gleis 25", "Gleis 26")

        val K_Deutz_9_10 = parts["8083368"]!!
        assertThat(K_Deutz_9_10).hasSize(2)
        assertThat(K_Deutz_9_10.map { it.trackName })
            .contains("Gleis 9", "Gleis 10")

        val K_Deutz_11_12 = parts["8073368"]!!
        assertThat(K_Deutz_11_12).hasSize(2)
        assertThat(K_Deutz_11_12.map { it.trackName })
            .contains("Gleis 11", "Gleis 12")

        // the additional tracks at Stolberg apparently are listed separately
        // because they don't belong to DB SuS
    }
}