package stations

import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.assertThat

internal class DbStationsTest {
    fun learnRegex123() {
        // sadly, I couldn't get this to work at all,
        // now using '.' or '[ -]' to match letters (thankfully, no more precision is needed)
        // doesn't match
        assertThat("ö".replace("""(?u)[\p{L}]""", "x")).isEqualTo("ö")
        // should match
        assertThat("ö".replace("""(?u)[\p{L}\p{M}]""", "x")).isEqualTo("x")
        assertThat("ß".replace("""\p{Alpha}""", "x")).isEqualTo("x")
    }

    /*
    Matching by name is only needed for less than 20 cases with differing DS100,
    but we can also use it for validating the DS100-matches!
    To avoid mismatches or losing useful data, we apply normalized rules separately to each data set.
    Can be changed easy enough if more flexibility is needed later.

    spec EVA:
     - space before '(' missing in EVA, I want to add it also to the displayed name,
        since infra always has it --> possibly already as part of parsing?
     - completely missing '()' expression in infra: remove from EVA in normalized name
     */
    @Test
    fun evaNormalization() {
        // this is what we do on reading the data, discarding the old spelling.
        assertThat(eva("Frankfurt(Oder)").name)
            .isEqualTo("Frankfurt (Oder)")

        assertThat(eva("Frankfurt(Oder)-Rosengarten").name)
            .isEqualTo("Frankfurt (Oder)-Rosengarten")

        assertThat(eva("Frankfurt(Main)Hbf").name)
            .isEqualTo("Frankfurt (Main) Hbf")

        assertThat(eva("Kläden(Kr Stendal)").name)
            .isEqualTo("Kläden (Kr Stendal)")

        assertThat(eva("Schönfließ(b Oranienburg)").name)
            .isEqualTo("Schönfließ (b Oranienburg)")

        assertThat(eva("Berlin Gesundbrunnen(S)").name)
            .isEqualTo("Berlin Gesundbrunnen (S)")
    }

    @Test
    fun evaAlternateSpelling() {
        // actually it's less about spelling and more about EVA being more precise,
        // which is partly because EVA has more stations (for example, three places named 'Hausen')
        // so EVA needs to disambiguate more with 'Hausen (Taunus)', 'Hausen (Eichsfeld)', and so on.
        // but in a few cases, there is only one variant and we find it in infra, but leaving the
        // '(...)' part out.
        // those normalizations should only apply when station is not found to avoid mismatches
        // or lost information.
        assertThat(eva("Wörth(Rhein) Zügelstraße").alternateSpelling)
            .isEqualTo("Wörth Zügelstraße")

        assertThat(eva("Schönfließ(b Oranienburg)").alternateSpelling)
            .isEqualTo("Schönfließ")
    }

    @Test
    fun infraNormalize() {
        assertThat(infra("München-Riem Pbf").name)
            .isEqualTo("München-Riem")
    }

    /*
     spec:
     - 'Kr ' at start of parenthesis in infra (25 cases of which 23 match in DS100),
        remove in normalized name.
     */
    @Test
    fun infraAlternateSpelling() {
        assertThat(infra("Kläden (Kr Stendal)").alternateSpelling)
            .isEqualTo("Kläden (Stendal)")
    }

    /*
    This is really tricky! And there is unfortunately at least one case where both sides need
    modifying to match:
    "Berlin-Schöneweide (S)" vs "Berlin-Schöneweide Pbf"
    --> should we normalize all 11 cases of " Pbf$" in SuS to get a match?
        (Instead of alternate spelling as we do now  --> information loss, but less complexity)
        (Side note: EVA only has one case: "Kornwestheim Pbf" which matches via DS100
         and then .longestName() preserves that form for the frontend.)
    --> alternative is to have a hashmap with alternate spellings, which we already rejected.

    also tricky:
    "Berlin Gesundbrunnen(S)" vs "Berlin-Gesundbrunnen"  (note: space vs dash)
    We have this often, but in all other cases can match via DS100!
     */
    @Test
    fun multistationNames() {
        assertThat(eva("München Hbf (tief)").multistationNames)
            .contains("München Hbf")

        assertThat(eva("München Hbf Gl.5-10").multistationNames)
            .contains("München Hbf")

        // unlike München, we have an extra space here
        assertThat(eva("Köln Messe/Deutz Gl. 9-10").multistationNames)
            .contains("Köln Messe/Deutz")

        assertThat(eva("Berlin Potsdamer Platz (S)").multistationNames)
            .contains("Berlin Potsdamer Platz")

        assertThat(eva("Berlin Potsdamer Platz (S)").multistationNames)
            .contains("Potsdamer Platz")

        assertThat(eva("Berlin Greifswalder Str").multistationNames)
            .contains("Greifswalder Straße")
    }

    @Test
    fun longestName() {
        val station = infra("Ostkreuz").apply {
            evaStations.add(eva("Berlin Ostkreuz"))
            evaStations.add(eva("Berlin Ostkreuz (S)"))
        }
        assertThat(station.longestName).isEqualTo("Berlin Ostkreuz")
    }

    private fun eva(name: String) = EvaStation.fromFile("id", listOf(), name, "RV")

    private fun infra(name: String) = InfraStation.fromFile("nr", "id", name)

}