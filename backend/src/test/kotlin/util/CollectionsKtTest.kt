package util

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import util.extract

internal class CollectionsKtTest {
    val predicate: (Int) -> Boolean = { it % 2 == 0}

    @Test
    fun extractEmpty() {
        val l = mutableListOf<Int>()
        val result = l.extract(predicate)
        assertThat(result).isEmpty()
    }

    @Test
    fun extractAll() {
        val l = mutableListOf(2, 4, 6, 444)
        val result = l.extract(predicate)
        assertThat(result).containsExactly(2, 4, 6, 444)
        assertThat(l).isEmpty()
    }

    @Test
    fun extractNone() {
        val l = mutableListOf(13, 17, 25)
        val result = l.extract(predicate)
        assertThat(result).isEmpty()
        assertThat(l).containsExactly(13, 17, 25)
    }

    @Test
    fun extractSome() {
        val l = mutableListOf(13, 12, 17, 25, 24)
        val result = l.extract(predicate)
        assertThat(result).containsExactly(12, 24)
        assertThat(l).containsExactly(13, 17, 25)
    }
}