const val timetableStationList = "data/DB_stations/D_Bahnhof_2017_09.csv"
const val infrastructureStationList = "data/DB_stations/DBSuS-Uebersicht_Bahnhoefe-Stand2018-03.csv"
const val platformList = "data/DB_stations/DBSuS-Bahnsteigdaten-Stand2018-03.csv"

val FRONTEND_STATIC_DATA_DIR = "../frontend/web/data/"