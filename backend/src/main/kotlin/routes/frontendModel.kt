package routes


// two data classes for the Json **route** output
data class LinesByNetwork(
    // indexed by Network name as defined in routes.GtfsSource
    val linesByNetwork: Map<String, LinesById>
)

data class LinesById(
    val linesById: Map<String, Line>
)


// three data classes for the Json file mapping from **stations to routes**
// besides the two files written by the DbStations backend, this is the third Json file for the
// client to read. It's a little redundant, but allows to keep the two backends separate.
data class LinesByStation(
    // indexed by EVA id (the only station id used in GTFS)
    val linesByStation: Map<String, LinesByStationByNetwork>
)

data class LinesByStationByNetwork(
    // Multimap indexed by Network name as defined in routes.GtfsSource
    // Values are Line.id
    val linesByNetwork: Map<String, List<String>>
)
