package routes

// classes only used for reading the CSV files
data class GtfsTrip(
    val routeId: String,
    val servicePattern: ServicePattern,
    val direction: Boolean
)

data class GtfsRoute(
    val id: String,
    val agency: String,
    val name: String,
    val type: String
) {
    // avoid warning for non-train routes
//    val longName: String by lazy { "$name ($agencyName)" }
    val longName: String = "$name ($id)"

    val isTrain: Boolean
        get() = type.startsWith("1") && type.length == 3

    val agencyName: String
        get() {
            val result = agencies[agency]
            if (result == null) {
                println("Unnamed agency: $agency.")
                return agency
            }
            return result
        }
}

val agencies = mapOf(
    "1" to "S-Bahn Berlin",
    "108" to "DB Regio",
    "472" to "Buckower Kleinbahn",
    "596" to "NEB",
    "731" to "ODEG",
    "751" to "Hanseatische Eisenbahn",
    "811" to "MRB"
)
