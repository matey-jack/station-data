package routes

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.DayOfWeek
import java.util.*
import kotlin.collections.HashMap

/*
A note for hacking on this: there's a lot of immutable classes containing other immutable classes
via apparently immutable collections (like List and Map). But although we use the minimal interfaces
[List] etc, which hide mutating functions, the underlying objects are often mutable and we use this
during loading time because adding things to lists is simpler code than doing it in immutable ways.

After the loading phase, however, all the objects are immutable and we intend to keep it that way.

See [MergedStation] and [MergedStationBuilder] for an example where immutability is enforced.

Note to self: Kotlin's [List] is not there to help with immutability (although it helps a bit at
least to document intent), but mostly to allow covariant types. Best practice for immutability seems
to make copies because creating dedicated immutable classes takes a lot of effort to get to the same
level of performance, functionality, and stability which the mutable classes have already reached.
Having private copies of data structures, only accessible in one file of code, helps enough to avoid
any unintended modification-mistakes.
*/

data class GtfsStation(
    val id: String,
    @JsonIgnore // see [name]
    val gtfsName: String
) {
    // need those for debugging
    @JsonIgnore
    val otherIds: MutableList<String> = mutableListOf()

    companion object {
        fun make(paddedId: String, name: String) = GtfsStation(paddedId.dropWhile { it == '0' }, name)

        val berlinSuffix = " Bhf (Berlin)"
    }

    val name = gtfsName
        .replace(", Bahnhof", "")
        .replace(", Hauptbahnhof", "")
        .replace(" ZUG", "")
        .frontStrip("S ")
        .frontStrip("S+U ")
        .let {
            if (it.endsWith(berlinSuffix)) {
                "Berlin-" + it.substring(0, it.length - berlinSuffix.length)
            } else {
                it
            }
        }

    private fun String.frontStrip(prefix: String): String =
        if (startsWith(prefix)) {
            substring(prefix.length)
        } else
            this
}

data class ServicePattern(
    val weekdays: EnumSet<DayOfWeek>
) {
    val numberWeekdays
        get() = weekdays.size
}

data class Route(
    // stops in Gtfs "1" direction
    @JsonIgnore
    val stops: List<GtfsStation>,

    // number of trips per regular week
    var count: Int
) {
    val stationString
        get() = stops.first().name + " <–> " + stops.last().name +
            " (${count} times per week, with ${stops.size} stops)"

    val allStations
        @JsonIgnore
        get() =
            "(${count} times per week) " +
                stops.joinToString(" – ") { it.name }

    @get:JsonIgnore
    val stopIds: Set<String> by lazy { stops.map { it.id }.toSet() }
}

// we are later going to serialize this is in form which fits the tabular
// display planned in the frontend. For example, line by line, which just Xs in the columns.
data class MergedStation(
    val station: GtfsStation,

    // We use each [Route]'s index in the [Line]'s [routes] list as index in this map,
    // since that list is sorted and not changed any more and it is also the index that will
    // later be needed in the frontend.
    @JsonIgnore
    val stoppingRoutes: BitSet,

    // indexed by GtfsStation.id
    // if we were crazy for performance, we could also assign ids to [MergedStation] (from 0,
    // in order of creation, locally for each Line) and then use a BitSet for the ordering.
    @JsonIgnore
    val previousStations: Set<String>
) : Comparable<MergedStation> {
    override fun compareTo(other: MergedStation): Int =
        when {
            previousStations.contains(other.station.id) -> +1
            other.previousStations.contains(station.id) -> -1
            else -> 0
        }

    // we publish "only" 31 routes per Line, because that's already far more than any of our
    // lines currently has, more than fits on a screen or makes sense to a user, but less than
    // JavaScripts 53 safe bits for an integer and even fitting in an unsigned int, just in case
    // a Browser-VM wants to optimize or some library in-between can't handle more.
    val stoppingRoutes31: Int
        get() = stoppingRoutes.toLongArray()[0].toInt()
}

/**
 * This class is separate from [GtfsRoute] for historical reasons (because at some point several
 * Routes were merged into a line, but that merged also some completely unrelated routes, so I
 * reverted it). But it's still good to have that separate class which might be useful when another
 * data source than GTFS comes along. It has also be proven useful for assigning nice human-readable
 * line-ids in an immutable fashion.
 */
data class Line(
    // id example: RE1a, RE1b, etc, or RB33 is there is only one by that name
    // Note that names are only unique within networks and a line which is included in two
    // different data sets can get different ids in both sets.
    val id: String,
    // name is RE1, RB33, S85, etc.
    val name: String,
    val network: String
) : Comparable<Line> {
    val type = name.takeWhile { it.isLetter() }

    val number = name
        .dropWhile { it.isLetter() }
        .takeWhile { it.isDigit() }
        .let { if (it == "") 0 else it.toInt() }

    val routes: MutableList<Route> = mutableListOf()

    override fun compareTo(other: Line): Int {
        if (other.type != type) {
            return type.compareTo(other.type)
        }
        if (other.number != number) {
            return number.compareTo(other.number)
        }
        return id.compareTo(other.id)
    }

    fun addTrip(trip: GtfsTrip, stations: MutableList<GtfsStation>) {
        if (!trip.direction) {
            stations.reverse()
        }
        val existing = routes.find { it.stops == stations }
        if (existing != null) {
            existing.count += trip.servicePattern.numberWeekdays
        } else {
            val route = Route(stations, trip.servicePattern.numberWeekdays)
            routes.add(route)
        }
    }

    val mergedRoute by lazy {
        mergeRoutes(routes.filter { it.count > 0 })
    }

    val longName
        get() = "${name} ${mergedRoute.first().station.name} " +
            "<–> ${mergedRoute.last().station.name}"
}

fun mergeRoutes(routes: List<Route>): List<MergedStation> {
    class NonlinearStationsException(
        val station: GtfsStation,
        val other: GtfsStation
    ) : RuntimeException()

    // We have this builder with a little code duplication here, because
    // [MergedStation.compareTo] should not depend on a mutable set.
    // alternative solutions welcome :))
    data class MergedStationBuilder(
        // for field documentation, see [MergedStation]
        val station: GtfsStation
    ) {
        val stoppingRoutes: BitSet = BitSet()
        val previousStations: MutableSet<String> = mutableSetOf()
        val subsequentStations: MutableSet<String> = mutableSetOf()

        // [toSet()] is important here because it copies the sets and thereby makes sure
        // that the result is guaranteed immutable, even if we continue to modify the builder.
        fun build() = MergedStation(
            station,
            stoppingRoutes.clone() as BitSet,
            previousStations.toSet()
        )
    }

    /**
     * Adds [other] as a previous station to [builder] and vice versa as subsequent station.
     * returns false if nothing has been added, so caller doesn't need to check again.
     */
    fun addPreviousSimply(builder: MergedStationBuilder, other: MergedStationBuilder): Boolean {
        val id = builder.station.id
        val otherId = other.station.id

        // we don't technically need this as base-case for any recursion,
        // but it allows us to skip a lot of unnecessary work.
        if (builder.previousStations.contains(otherId)) return false

        if (builder.subsequentStations.contains(otherId) || other.previousStations.contains(id)) {
            throw NonlinearStationsException(builder.station, other.station)
        }
        builder.previousStations.add(otherId)
        other.subsequentStations.add(id)
        return true
    }

    // indexed by Gtfs station id
    // if we were loving speed, we could make this a list, and use indexes as ids for
    // [MergedStationBuilder]. We wouldn't even need to store the ids in the objects, if we
    // passed ids everywhere, instead of objects. That would definitely save a few µs or at
    // ns of total run time.
    val builders = HashMap<String, MergedStationBuilder>()

    fun addPrevious(builder: MergedStationBuilder, other: MergedStationBuilder) {
        if (!addPreviousSimply(builder, other)) return

        // since our matrix is a transitive closure by invariant, we only need to add one level
        // deep each time. no recursion needed.
        other.previousStations.forEach {
            addPreviousSimply(builder, builders[it]!!)
        }
        builder.subsequentStations.forEach {
            addPreviousSimply(builders[it]!!, other)
        }
    }

    try {
        routes.forEachIndexed { routeId, route ->
            route.stops.forEachIndexed { i, station ->
                // TODO since MDV data does not contain proper [direction] for trips,
                // we have to try to apply a route and if it leads to an inconsistency, reverse it
                // and apply again. There might be cases, where this will fail (it might even be
                // algorithmically very hard in the general case). We can increase chances of success
                // by starting with the longest routes first!
                val mergedStation = builders.getOrPut(station.id) { MergedStationBuilder(station) }
                mergedStation.stoppingRoutes.set(routeId)
                route.stops.subList(0, i).forEach { addPrevious(mergedStation, builders[it.id]!!) }
            }
        }
    } catch (e: NonlinearStationsException) {
        println("ERROR: inconsistent stops on route: ${e.station.name} and ${e.other.name}")
        return listOf()
    }

    return try {
        builders.values.map {
            it.build()
        }.sorted()
    } catch (e: IllegalArgumentException) {
        println("ERROR: we need to catch this better. TODO TODO TODO")
        e.printStackTrace()
        listOf()
    }
}