package routes

import FRONTEND_STATIC_DATA_DIR
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import stations.DbStations
import util.flipKeys
import java.io.File
import java.nio.file.Path
import java.nio.file.Paths
import java.time.DayOfWeek
import java.util.*
import kotlin.collections.HashMap

data class GtfsSource(
    val path: Path,
    val network: String
) {
    val tripsDirectionIndex: Int = if (network == "MDV") 4 else 5
    val stopsNameIndex: Int = if (network == "MDV") 1 else 2

    val fixedServicePattern: ServicePattern? =
        if (network == "MDV")
            ServicePattern(EnumSet.allOf(DayOfWeek::class.java))
        else
            null
}

class GtfsDataSet(
    val source: GtfsSource
) {
    // only really skips header record when we also set some [header]
    val csvFormat = CSVFormat.DEFAULT.withSkipHeaderRecord().withHeader("")!!

    val stations = HashMap<String, GtfsStation>()
    fun getStationById(maybePaddedId: String): GtfsStation? {
        val id = maybePaddedId.dropWhile { it == '0' }
        return stations[id]
    }

    /*
    We translate GtfsRoute to our own concept of "Line" by grouping all the routes with the same name,
    but filtering out those which are bus replacement service.

    And we're translating GtfsTrip to our own "Route" by omitting stop-times and aggregation all trips
    with the same stopping pattern (= list of stops).

    Filter GtfsRoute as we read them, later ignore all GtfsTrips with routeIds that we already dropped.

    In memory, keep only "Line" and "Route" objects.
     */

    // those three Maps represent the data read from the source files
    val tripsById = HashMap<String, GtfsTrip>()
    val servicesById = HashMap<String, ServicePattern>()

    // we don't store routeId anywhere except keys of this map, because we only need it to assign
    // trips to the line.
    val linesByRouteId = HashMap<String, Line>()

    // TODO: This is currently not needed, because we have one Line = one GtfsRoute.
    // But won't refactor it until we're sure it'll stay that way.
    val linesById = HashMap<String, Line>()

    fun readGtfsStations() {
        // Apparently, VBB has the same station appear several times with different ids
        // and it seems that the smallest of those ids is EvaId equal to the one
        // in station data. To deal with this, we read stations data into two Maps and at
        // the end, collapse them into one. Thereby we keep all ids as keys to the resulting
        // map, but the objects themselves will only exist once per station name, with the
        // (hopefully) canonical id.
        // (Those copies of a station also are linked by a parentId, but since the station name
        // seems to be unambiguous we spare ourselves the complexity of reading and storing that
        // extra field.

        fun shorterString(a: String, b: String) = if (a.length < b.length) a else b

        // this map makes sure we have exactly one station object for each stationName
        val minimalIdByName = HashMap<String, String>()

        // this is needed in the end to create the resulting map which has all ids in its
        // key set (not just the canonical ones)
        val namesById = HashMap<String, String>()

        val csvData = source.path.resolve("stops.txt").toFile()
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser.records) {
                val id = csvRecord[0].dropWhile { it == '0' }
                val name = csvRecord[source.stopsNameIndex]
                namesById[id] = name
                minimalIdByName.merge(name, id, ::shorterString)
            }
        }

        // create station objects once
        minimalIdByName.forEach { name, evaId ->
            stations[evaId] = GtfsStation.make(evaId, name)
        }

        // refer them from redundant Ids
        namesById.forEach { id, name ->
            // note that we also overwrite each station with itself once
            // but that's easier than catching this case upfront.
            val station = stations[minimalIdByName[name]]!!
            stations[id] = station
            if (station.id != id) station.otherIds.add(id)
        }
    }

    fun readRoutes(): List<GtfsRoute> {
        val result = ArrayList<GtfsRoute>()
        val csvData = source.path.resolve("routes.txt").toFile()
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser) {
                val route = GtfsRoute(csvRecord[0], csvRecord[1], csvRecord[2], csvRecord[4])
                if (route.isTrain) {
                    result.add(route)
                }
            }
        }
        return result
    }

    fun createLine(route: GtfsRoute, suffix: String) {
        val line = Line(route.name + suffix, route.name, source.network)
        linesById[line.id] = line
        linesByRouteId[route.id] = line
    }

    fun createLines(routes: List<GtfsRoute>) {
        // Although each [Line] corresponds to exactly one [GtfsRoute] we only create [Line] objects
        // after reading all the Routes because we want to create unambiguous yet human-readable
        // ids.
        routes.groupBy { it.name }.forEach { _, homonymousRoutes ->
            if (homonymousRoutes.size == 1) {
                createLine(homonymousRoutes.first(), "")
            } else {
                homonymousRoutes.forEachIndexed { index, route ->
                    createLine(route, ('a' + index).toString())
                }
            }
        }
    }

    fun readTripIds() {
        val csvData = source.path.resolve("trips.txt").toFile()
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser) {
                val routeId = csvRecord[0]
                val servicePattern = source.fixedServicePattern ?: servicesById[csvRecord[1]]
                val tripId = csvRecord[2]
                val direction = csvRecord[source.tripsDirectionIndex] == "1"
                if (servicePattern != null && linesByRouteId.containsKey(routeId)) {
                    tripsById[tripId] = GtfsTrip(routeId, servicePattern, direction)
                }
            }
        }
    }

    /**
     * Read trips and add them to the appropriate route.
     */
    fun readTripStations() {
        // Since this file is really big, we process it in one pass, assuming that data is grouped
        // by tripId. (Assumption is checked on the way.)
        val csvData = source.path.resolve("stop_times.txt").toFile()
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            var currentTripId: String? = null
            var trip: GtfsTrip? = null
            var currentStations: MutableList<GtfsStation> = ArrayList()
            val finishedTrips = HashSet<String>()
            for (csvRecord in parser) {
                val tripId = csvRecord[0]
                if (currentTripId == null) {
                    currentTripId = tripId
                    trip = tripsById[currentTripId]
                } else if (currentTripId != tripId) {
                    assert(tripId !in finishedTrips) {
                        "tripId occurring twice, not grouped together! "
                    }
                    if (trip != null) {
                        linesByRouteId[trip.routeId]?.addTrip(trip, currentStations)
                    }
                    finishedTrips.add(currentTripId)
                    currentTripId = tripId
                    trip = tripsById[currentTripId]
                    currentStations = ArrayList()
                }
                // ignore stations for trips which we ignore
                if (trip != null) {
                    val station = getStationById(csvRecord[3])
                    // ignore stations for which we don't have a name
                    if (station != null) {
                        currentStations.add(station)
                    }
                }
            }
        }
    }

    fun readServicePatterns() {
        val csvData = source.path.resolve("calendar.txt").toFile()
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser) {
                val serviceId = csvRecord[0]
                // Regular trips run at least on one day of the week.
                val weekdays = EnumSet.noneOf(DayOfWeek::class.java)
                (1..7).forEach {
                    if (csvRecord[it] == "1") {
                        weekdays.add(DayOfWeek.of(it))
                    }
                }
                servicesById[serviceId] = ServicePattern(weekdays)
            }
        }
    }

    fun readAllTheThings() {
        // need to read Calendar first because we filter trips on reading them, using the calendar
        readServicePatterns()
        readGtfsStations()
        // Routes must be read before trips
        val gtfsRoutes = readRoutes()
        createLines(gtfsRoutes)
        readTripIds()
        // this is last because it needs trips and stations
        readTripStations()

        // sort this once, because we need it often later
        linesById.values.forEach {
            it.routes.sortByDescending { it.count }
        }
    }

    fun printLines() {
        linesById.values
            .sorted()
            .forEach { line ->
                println("${line.id}:")
                printLineWithAllRoutes(line)
                printLineWithAllStops(line)
            }
    }

    fun printLineWithAllRoutes(line: Line) {
        line.routes.sortedByDescending { it.count }
            .forEach { route ->
                println("\t" + route.stationString)
            }
    }

    fun stopsAt(routeId: Int, route: Route, station: MergedStation): Boolean =
        if (route.count == 0)
            route.stopIds.contains(station.station.id)
        else
            station.stoppingRoutes.get(routeId)

    fun printLineWithAllStops(line: Line) {
        val firstRoutes = line.routes.take(5)
        val counts = firstRoutes.joinToString(" ") { it.count.toString() }
        println("Runs per week: $counts")
        line.mergedRoute.forEach { mergedStation ->
            val xs = firstRoutes.mapIndexed { index, route ->
                if (stopsAt(index, route, mergedStation)) "X" else " "
            }.joinToString(" ")

            val station = mergedStation.station
            println(" %s  %30s  -  %s".format(xs, station.name, station.id))
        }
        println()
    }

    /**
     * Returns a multimap from Station-Id to Line-Id.
     */
    fun getStationRoutes(): Map<String, List<String>> {
        val result = HashMap<String, MutableList<String>>()
        linesByRouteId.values.forEach { line ->
            line.mergedRoute.forEach { station ->
                result.getOrPut(station.station.id) { ArrayList() }.add(line.id)
            }
        }
        // for each station, sort the list of lines
        result.values.forEach { it.sort() }
        return result
    }

    fun printAllRouteStopsOnLine(lineId: String) {
        println()
        println("----------------- *** $lineId ***-------------------------")
        val line = linesById[lineId]!!
        line.routes.forEach { route ->
            //            println(route.allStations)
            println(" ---------- ${route.count} times per week -----------------")
            route.stops.forEach { println(it.name) }
        }
    }
}

fun printTripWithStations(trip: Route, dbStations: DbStations) {
    trip.stops.forEach { stop ->
        val dbStation = dbStations.stationsByEvaId[stop.id]
        if (dbStation == null) {
            println(stop.name + " (keine DB Station gefunden)")
        } else {
            println(dbStation.shortString)
        }
    }
}

fun writeRoutesJson(allDataSets: List<GtfsDataSet>,
                    fileName: String,
                    objectMapper: ObjectWriter) {
    val networks = allDataSets.map { dataSet ->
        dataSet.source.network to LinesById(dataSet.linesById.filter { it.value.mergedRoute.isNotEmpty() })
    }.toMap()
    objectMapper.writeValue(File(fileName), LinesByNetwork(networks))
}

fun writeStationsJson(allDataSets: List<GtfsDataSet>,
                      fileName: String,
                      objectMapper: ObjectWriter) {
    val refsByNetwork = allDataSets.map { dataSet ->
        dataSet.source.network to dataSet.getStationRoutes()
    }.toMap()
    // we created the data by network, but need it by station, thus flipKeys.
    val stationRoutes = flipKeys(refsByNetwork).mapValues {
        LinesByStationByNetwork(it.value)
    }
    objectMapper.writeValue(File(fileName), LinesByStation(stationRoutes))
}

fun writeJson(allDataSets: List<GtfsDataSet>) {
    val objectMapper = jacksonObjectMapper().writer() //.writerWithDefaultPrettyPrinter()
    writeRoutesJson(allDataSets, FRONTEND_STATIC_DATA_DIR + "allRoutes.json", objectMapper)
    writeStationsJson(allDataSets, FRONTEND_STATIC_DATA_DIR + "stationsToRoutes.json", objectMapper)
}

val sources = listOf(
    GtfsSource(Paths.get("data/gtfs/vbb_2019H1"), "VBB")
//    GtfsSource(Paths.get("../../gtfs/mdv_all"), "MDV")
)

fun readDataSets(sources: List<GtfsSource>): List<GtfsDataSet> {
    val result = mutableListOf<GtfsDataSet>()
    sources.forEach { source ->
        try {
            val dataSet = GtfsDataSet(source)
            dataSet.readAllTheThings()
            result.add(dataSet)
        } catch (e: Exception) {
            println("Failed to read GTFS data of '${source.network}'.")
            e.printStackTrace()
        }
    }
    return result
}

fun main(args: Array<String>) {
//    val br = BufferedReader(InputStreamReader(System.`in`))
//    br.readLine()

    val allDataSets = readDataSets(sources)
    for (dataSet in allDataSets) {
        dataSet.printLines()
//        dataSet.printAllRouteStopsOnLine("S4")
//        dataSet.printLineWithAllStops(dataSet.linesById["S4"]!!)
    }

    writeJson(allDataSets)
    println()
//    br.readLine()
}