package stations.statistics

import infrastructureStationList
import org.apache.commons.csv.CSVParser
import stations.DbStations
import java.io.File

val oldRegions = mapOf(
    'A' to "Hamburg (Altona)",
    'B' to "Berlin",
    'D' to "Dresden",
    'E' to "Essen",
    'F' to "Frankfurt",
    'H' to "Hannover",
    'K' to "Köln",
    'L' to "Halle (Leipzig)",
    'M' to "München",
    'N' to "Nürnberg",
    'R' to "Karlsruhe",  // Mnemonic: Rhein (Oberrhein)
    'S' to "Saarbrücken",
    'T' to "Stuttgart",  // Mnemonic: Tannenwald
    'U' to "Erfurt",     // Mnemonic: Unstrut
    'W' to "Schwerin"    // Mnemonic: Wasser
)

data class Region(
    val bundesland: String,
    val regionGrob: String,
    val zentraleStadt: String,
    val aufgabenträger: String,
    val ds100: String
)

// As opposed to an in-place lambda, we need to specify all the parameters and types.
// A shared lambda (assigned to a variable instead of a call-site) allows to use _
// for unused parameters, but still requires us to declare their types.
// And genericity seems to be incompatible with the ::functionName calling syntax.
fun addValues(key: String, acc: Int?, el: Map.Entry<Region, Int>, first: Boolean) =
    el.value + (acc ?: 0)


fun main() {
    val csvData = File(infrastructureStationList)
    // Note on performance: Region instances will compute their hashcode() on every access
    // of the Map (although at least, underlying String caches its hashcode).
    // As of Kotlin in 2018-12 we'd need to manually reimplement hashcode() if we wanted
    // to cache its value.
    val counts = HashMap<Region, Int>()
    CSVParser.parse(csvData, Charsets.UTF_8, DbStations.csvFormat).use { parser ->
        for (csvRecord in parser) {
            // Header: Bundesland;RB;BM;Bf. Nr.;Station;Bf DS 100 Abk.;Kat. Vst;Straße;PLZ;Ort;Aufgabenträger
            val bundesland = csvRecord[0]
            val regionGrob = csvRecord[1]
            val zentraleStadt = csvRecord[2]
            val ds100 = csvRecord[5]
            val aufgabenträger = csvRecord[10]
            val region = Region(bundesland, regionGrob, zentraleStadt, aufgabenträger, ds100)
            val c = counts.getOrDefault(region, 0)
            counts[region] = c + 1
        }
    }

    println("Gesamtzahl der Stationen: ${counts.values.sum()}")

    println("\n----------------------------------------------------------------")
    println("Organisation der Regionalbereiche DB Station und Service\n")
    counts.entries
        .groupingBy { it.key.regionGrob + "  ~~  " + it.key.zentraleStadt }
        .aggregate(::addValues)
        .entries
        .sortedBy { it.key }
        .forEach { println("% 5d  %s".format(it.value, it.key)) }

    println("\n----------------------------------------------------------------")
    println("Aufgabenträger der Bundesländer für die Bestellung von Nahverkehr\n")
    counts.entries
        .groupingBy { it.key.bundesland + "  ~~  " + it.key.aufgabenträger }
        .aggregate(::addValues)
        .entries
        .sortedBy { it.key }
        .forEach { println("% 5d  %s".format(it.value, it.key)) }

    val regionOrder = "Ost Nord West Mitte Südost Süd Südwest".split(' ')
        .mapIndexed { i, n -> n to i }
        .toMap()

    fun regionIndex(region: Region) =
        regionOrder[region.regionGrob.substring(3)].toString()

    println("\n----------------------------------------------------------------")
    println("Gegenüberstellung Bahn-Regionen und Bundesländer\n")
    counts.entries
        .groupingBy { regionIndex(it.key) + it.key.regionGrob + "  ~~  " + it.key.bundesland }
        .aggregate(::addValues)
        .entries
        .sortedBy { it.key }
        .forEach { println("% 5d  %s".format(it.value, it.key.substring(1))) }

    println("\n----------------------------------------------------------------")
    println("Gegenüberstellung Bahnzentralen und Bundesländer\n")
    counts.entries
        .groupingBy { regionIndex(it.key) + it.key.regionGrob + "  ~~  " + it.key.bundesland + "  ~~  " + it.key.zentraleStadt }
        .aggregate(::addValues)
        .entries
        .sortedBy { it.key }
        .forEach { println("% 5d  %s".format(it.value, it.key.substring(1))) }

    println("\n----------------------------------------------------------------")
    println("Historische Reichsbahndirektionen\n")
    counts.entries
        .groupingBy { it.key.ds100.first() }
        // use a lambda here, because type of unused variable is different from function declared above
        .aggregate { _, acc: Int?, el: Map.Entry<Region, Int>, _ -> el.value + (acc ?: 0) }
        .entries
        .sortedBy { it.key }
        .forEach {
            println("% 5d  %s".format(it.value, oldRegions[it.key]))
        }
}
