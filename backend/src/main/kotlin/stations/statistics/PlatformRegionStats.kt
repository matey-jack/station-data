package stations.statistics

import stations.DbStations.Companion.csvFormat
import infrastructureStationList
import mu.KLogging
import org.apache.commons.csv.CSVParser
import platformList
import java.io.File
import java.util.*
import java.io.FileWriter
import java.io.BufferedWriter


data class Station(
    val bundesland: String,
    val regionalZentrum: String,
    val platformsHeights: MutableList<Int> = mutableListOf()
)

class PlatformRegionStats {
    companion object : KLogging()

    val stationsByBfNr: MutableMap<String, Station> = mutableMapOf()

    fun readStations() {
        val csvData = File(infrastructureStationList)
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser.records) {
                // Header: 0   1  2  3       4          5           6
                // Bundesland;RB;BM;Bf. Nr.;Station;Bf DS 100 Abk.;Kat. Vst;Straße;PLZ;Ort;Aufgabenträger
                val bundesland = csvRecord[0]
                val regionalZentrum = csvRecord[2]
                val bfNr = csvRecord[3]
                val station = Station(bundesland, regionalZentrum)
                stationsByBfNr[bfNr] = station
            }
        }
    }

    fun readPlatforms() {
        val csvData = File(platformList)
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser) {
                val bfNr = csvRecord[0]
                val station = stationsByBfNr[bfNr]
                if (station != null) {
                    val height = csvRecord[5].takeWhile { it.isDigit() }.toInt()
                    station.platformsHeights.add(height)
                } else {
                    logger.debug { "Skipping platforms for unknown station with Bf.Nr. $bfNr." }
                }
            }
        }
    }

    val heights = listOf("< 38", "38", "55", "76", "96", "sonstige")

    fun heightGroup(height: Int): String {
        val h = if (height < 38) "< 38" else height.toString()
        return if (h in heights) h else "sonstige"
    }

    fun sortedMap(): SortedMap<String, Int> = TreeMap { a, b ->
        if (a == b) 0
        else if (a.startsWith('<')) -1
        else if (b.startsWith('<')) +1
        else a.toInt().compareTo(b.toInt())
    }

    private fun countGroupsBy(keyName: String, groupingKey: (Station) -> String) {
        val countsByLand = stationsByBfNr.values
            .groupingBy(groupingKey)
            .aggregate { _, heightCounts: Map<String, Int>?, station, _ ->
                station.platformsHeights.fold(heightCounts ?: mapOf()) { counts, h ->
                    val g = heightGroup(h)
                    counts.plus(g to counts.getOrDefault(g, 0) + 1)
                }
            }

        BufferedWriter(FileWriter("data/DB_stations/${keyName}-stats.csv"))
            .use { writer ->
                print("%25s".format(keyName))
                writer.write(keyName)
                heights.forEach { height ->
                    print("%5s".format(height))
                    writer.write(";$height")
                }
                println()
                writer.newLine()
                countsByLand.forEach { bundesland, heightCounts ->
                    print("%25s".format(bundesland))
                    writer.write(bundesland)
                    heights.forEach { height ->
                        val count = heightCounts.getOrDefault(height, 0)
                        print("%5d".format(count))
                        writer.write(";$count")
                    }
                    println()
                    writer.newLine()
                }
            }
    }

    fun main() {
        readStations()
        readPlatforms()
        countGroupsBy("Bundesland") { it.bundesland }
        println("-----------------")
        countGroupsBy("Region") { it.regionalZentrum }
    }
}

fun main() {
    PlatformRegionStats().main()
}