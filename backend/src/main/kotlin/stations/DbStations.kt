package stations

import FRONTEND_STATIC_DATA_DIR
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import util.extract
import infrastructureStationList
import mu.KLogging
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import platformList
import timetableStationList
import java.io.File

data class Platform(
    val trackName: String,
    val platformName: String,
    val length: Int,
    val height: Int
) {
    val trackNumber: Int
        @JsonIgnore
        // only used for filtering in multistations, so we can afford to not
        // return a sensible value in all other cases.
        get() {
            val num = trackName.split(" ")[1].takeWhile { it.isDigit() }
            return if (num.isEmpty()) 0 else num.toInt()
        }
}

/*
The timetable-file (eva-list) has more stations, which I think is because more than 1000 stations
do not belong to 'DB Station und Service' (the national infrastructure company).
There are also some larger stations which have multi-entries on the eva-list and only one entry
on the infrastructure list. We collect all those entries together into a "multi-station" entry.
This is why the InfraStation data class owns a list of EvaStations.
 */
data class EvaStation private constructor(
    val evaId: String,
    val ds100: List<String>,
    val name: String,
    val verkehr: String,
    val routes: List<String>
) {
    companion object {
        fun fromFile(evaId: String, ds100: List<String>, nameInFile: String, verkehr: String) =
            nameInFile
                // add spaces befor and after parenthesis.
                .replace(Regex("""([^ ])\("""), """$1 (""")
                .replace(Regex("""\)([^ -])"""), """) $1""")
                .let { name -> EvaStation(evaId, ds100, name, verkehr, mutableListOf()) }

    }

    // only match with this name when the original one is not found
    val alternateSpelling: String
        @JsonIgnore
        get() {
            val alt = name.replace(Regex(""" \([^)]+\)"""), "")
            // don't remove the parenthesized term when it seems to be a multistation,
            // since we want to treat them separately.
            return if (alt in multistationNames) name else alt
        }

    // this is for the case of two stations in one; we need to normalize conservatively,
    // since this will be applied even when a match has already been found!!
    val multistationNames: Set<String>
        @JsonIgnore
        get() {
            // this case is so special that hard-coding is best
            if (name == "Frankfurt Hbf (tief)") {
                return setOf("Frankfurt (Main) Hbf")
            }

            val sansSuffix = name
                .replace(Regex(""" Gl[.] ?\d\d?(-\d\d?)?$"""), "")
                .replace(Regex(""" \(S\)$"""), "")
                .replace(Regex(""" \(S-Bahn\)$"""), "")
                .replace(Regex(""" \(tief\)$"""), "")
                .replace(Regex(""" Str$"""), " Straße")
            val sansPrefix = sansSuffix
                .replace(Regex("""^Berlin[ -]"""), "")
                .replace(Regex("""^Hamburg[ -]"""), "")

            // currently only need that for "Berlin Gesundbrunnen", but completeness rules.
            val alternatePrefix = sansSuffix
                .replace(Regex("""^Berlin """), "Berlin-")

            return if (sansPrefix == "Hbf") setOf(sansSuffix) else setOf(sansSuffix, sansPrefix, alternatePrefix)
        }

}

data class InfraStation private constructor(
    val bfNummer: String,  // unique identifier
    val ds100: String,    // also unique (and always present) in source file, but see evaStations:ds100
    val name: String,
    val evaStations: MutableList<EvaStation> = mutableListOf(),
    val platforms: MutableList<Platform> = mutableListOf()
) {
    companion object {
        fun fromFile(bfNummer: String, ds100: String, nameInFile: String) =
            nameInFile
                .replace(Regex(" Pbf$"), "")
                .let { name -> InfraStation(bfNummer, ds100, name, mutableListOf(), mutableListOf()) }
    }

    val alternateSpelling: String
        @JsonIgnore
        get() = name
            .replace("(Kr ", "(")

    /**
     * Using the longer of the EVA and infra names usually gives the more descriptive and unambiguous name.
     *
     * In multi-stations we use the shortest EVA name to compare with the infra name. (This rids us of special cases
     * like an "S-Bahn" suffix or "Gl.10-20".)
     *
     * There are 220 cases where EVA is longer: Many of them in Berlin and Hamburg S-Bahn,
     * but also many outside. Some especially useful, like 'Halle Zoo' which in infra is just 'Zoo'.
     * And 236 where infra is longer.
     *
     * There's at least one case, where the names are so different that both would be
     * worthwhile to be autocompleted in the frontend:
     *     Yorckstraße (Großgörschenstraße)  -->  Berlin Yorckstr. (S1)
     * But that doesn't yet warrant to add more complicated logic here.
     */
    val longestName: String
        @JsonIgnore
        get() {
            // evaStations might be empty
            val evaName = evaStations.minBy { it.name.length }.name
            // this list has 2 elements, so maxBy can't be null
            return listOf(name, evaName).maxBy { it.length }
        }

    val allNames: Set<String>
        @JsonIgnore
        get() = evaStations.map { it.name }.toSet().plus(name)

    val shortString: String
        @JsonIgnore
        get() {
            val pfLengths = platforms.map { it.length }.sorted()
            return "$name - " + when {
                platforms.isEmpty() -> "No platform information available."
                platforms.size == 1 -> "One platform of length ${platforms.first().length} m."
                else -> "${platforms.size} platforms from ${pfLengths.first()} to ${pfLengths.last()} m."
            }
        }

    val platformHeightsSummary: String
        @JsonIgnore
        get() {
            val pfLengths = platforms.map { it.height }.sorted()
            if (pfLengths.isEmpty()) {
                return "none"
            }
            if (pfLengths.last() <= 38) {
                return "all <= 38"
            }
            if (pfLengths.first() == pfLengths.last()) {
                return "all ${pfLengths.first()}"
            }
            if (pfLengths.first() <= 38) {
                return "<=38 to ${pfLengths.last()}"
            }
            return "${pfLengths.first()} to ${pfLengths.last()}"
        }

    private var _totalPlatformLength: Int = 0

    /**
     * only call this after all platforms have been loaded
     */
    val totalPlatformLength: Int
        @JsonIgnore
        get() {
            if (_totalPlatformLength == 0 && !platforms.isEmpty()) {
                _totalPlatformLength = platforms.map { it.length }.sum()
            }
            return _totalPlatformLength
        }
}

// so we can create Json with a top-level object (best practice and more extendable)
// instead of a list or dynamic map.
data class StationsWrapper(
    // index is "longName", just as in stations.StationNamesWrapper
    val stations: Map<String, InfraStation>,

    // evaId to long name which indexes the previous map
    val evaIdToStationName: Map<String, String>,

    // since less than 1% of stations are multistations, we don't put this info on the above classes.
    // joining of data happens via evaId in stations.StationsWrapper.multistationParts[keyspace]
    // index is evaId
    val multistationParts: Map<String, List<Platform>>
)

data class StationNamesWrapper(val stationNames: List<String>)

class DbStations {
    companion object : KLogging() {
        // only really skips header record when we also set some [header]
        val csvFormat: CSVFormat =
            CSVFormat.newFormat(';').withSkipHeaderRecord().withHeader("")
    }

    // those are temporary maps for matching EVA + Infra
    // we removed matched stations from this one, so that the laxer matching rules used at the end
    // don't create additional wrong matchings
    val evaByName = HashMap<String, EvaStation>()

    // this has duplicate entries because of duplicate DS100 ids on some stations
    val evaByDs100 = HashMap<String, EvaStation>()

    // ------------------------ //

    // this is the unique index (all stations, each once)
    val stationsByBfNr = HashMap<String, InfraStation>()

    // no entry for unmatched stations, multiple entries for multi-stations
    val stationsByEvaId = HashMap<String, InfraStation>()

    // unique entries using the infrastation.name
    val stationsByName = HashMap<String, InfraStation>()

    // not used yet
    val stationsByDs100 = HashMap<String, InfraStation>()

    val multistations: List<InfraStation>
        get() = stationsByBfNr.values.filter { it.evaStations.size > 1 }

    private fun addEvaStation(s: EvaStation) {
        evaByName[s.name] = s
        s.ds100.forEach { evaByDs100[it] = s }
    }

    private fun addEvaToStation(infraStation: InfraStation, evaStation: EvaStation) {
        evaByName.remove(evaStation.name)
        infraStation.evaStations.add(evaStation)

        stationsByEvaId[evaStation.evaId] = infraStation
        evaStation.ds100.forEach {
            stationsByDs100[it] = infraStation
        }
    }

    private fun addStation(s: InfraStation) {
        // store a normalized name, so unmatched evaStations can find it.
        // only applies to Lutherstadt Wittenberg and Berlin
        // only really needed for Berlin
        stationsByName[s.name.replace(" Hauptbahnhof", " Hbf")] = s

        stationsByDs100[s.ds100] = s
        s.evaStations.forEach { stationsByEvaId[it.evaId] = s }
        stationsByBfNr[s.bfNummer] = s
    }

    /*
    We can create a InfraStation object from just one of the two files or merge information from both files.
    This way, we'll be able to find and display stations even if we don't have route info or platform info
    for them.
    Maybe it also makes merging easier.
     */
    fun readEvaStations() {
        val csvData = File(timetableStationList)
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser) {
                //           0      1       2   3      4      5      6      7
                // Header: EVA_NR;DS100;IFOPT;NAME;VERKEHR;LAENGE;BREITE;STATUS
                val evaId = csvRecord[0]
                val ds100s = csvRecord[1].split(',')
                val name = csvRecord[3]
                addEvaStation(EvaStation.fromFile(evaId, ds100s, name, csvRecord[4]))
            }
        }
    }

    private fun findEvaStation(station: InfraStation): EvaStation? {
        // matching by DS100 is the default and finds > 90% of all stations
        var evaStation = evaByDs100[station.ds100]
        if (evaStation != null) {
            if (station.name != evaStation.name
                && station.alternateSpelling != evaStation.name
                && station.name != evaStation.alternateSpelling
                && station.name !in evaStation.multistationNames
            ) {
                logger.debug("Name mismatch EVA: ${evaStation.name} versus infra: ${station.name}")
            }
            return evaStation
        }

        // match by name
        evaStation = evaByName[station.name]
        if (evaStation == null) {
            evaStation = evaByName[station.alternateSpelling]
        }
        if (evaStation != null) {
            logger.debug("DS100 mismatch for ${evaStation.name}: " +
                "EVA: ${evaStation.ds100} versus infra: ${station.ds100}")
        }
        return evaStation
    }

    fun readInfraStations() {
        val csvData = File(infrastructureStationList)
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser.records) {
                // Header: 0   1  2  3       4          5           6
                // Bundesland;RB;BM;Bf. Nr.;Station;Bf DS 100 Abk.;Kat. Vst;Straße;PLZ;Ort;Aufgabenträger
                val ds100 = csvRecord[5]
                val name = csvRecord[4]
                val station = InfraStation.fromFile(csvRecord[3], ds100, name)
                val evaStation = findEvaStation(station)
                if (evaStation != null) {
                    addEvaToStation(station, evaStation)
                }
                addStation(station)
            }
        }
    }

    fun readPlatforms() {
        val csvData = File(platformList)
        CSVParser.parse(csvData, Charsets.UTF_8, csvFormat).use { parser ->
            for (csvRecord in parser) {
                val bfNr = csvRecord[0]
                val station = stationsByBfNr[bfNr]
                if (station != null) {
                    val length = csvRecord[4].takeWhile { it.isDigit() }.toInt()
                    val height = csvRecord[5].takeWhile { it.isDigit() }.toInt()
                    val platform = Platform(csvRecord[3], csvRecord[1], length, height)
                    station.platforms.add(platform)
                } else {
                    logger.debug { "Skipping platforms for unknown station with Bf.Nr. $bfNr." }
                }
            }
        }
    }

    private fun matchUnmatchedEvaStations() {
        // 'toList()' copies the values to iterate, so that we can concurrently modify the HashMap
        evaByName.values.toList()
            .forEach { evaStation ->
                // single match in case of different spelling
                val infraStation = stationsByName[evaStation.alternateSpelling]
                // make sure, we do not create extra multi-stations. We want to be restrictive,
                // because the alternate spelling drops a qualifier that in some cases actually
                // makes an important distinction.
                if (infraStation != null && infraStation.evaStations.isEmpty()) {
                    logger.debug("DS100 mismatch for ${evaStation.name}: " +
                        "EVA: ${evaStation.ds100} versus infra: ${infraStation.ds100}")
                    addEvaToStation(infraStation, evaStation)
                    return@forEach
                }
                // still not found: maybe this is part of a multi-station?
                evaStation.multistationNames
                    .mapNotNull { stationsByName[it] }
                    .forEach {
                        addEvaToStation(it, evaStation)
                    }
            }

    }

    fun readAllTheThings() {
        readEvaStations()
        readInfraStations()
        readPlatforms()
        matchUnmatchedEvaStations()
    }

    fun printUnmatched() {
        // first show unmatched infra-stations, because those are rare:
        println("Stationen der DB SuS ohne Eintrag im EVA")
        stationsByBfNr.values
            .filter { it.evaStations.isEmpty() }
            .sortedBy { it.ds100 }
            .forEach {
                println("%6s %s".format(it.ds100, it.name))
            }
        println()

        // then show unmatched eva-stations (matching removed them from this map)
        val verkehrOrder = listOf("FV", "RV", "nur DPN")
            .mapIndexed { i, n -> n to i }
            .toMap()

        println("Stationen der EVA ohne Eintrag bei DB SuS")
        evaByName.values
            .sortedBy { it.ds100.firstOrNull() ?: "" }
            .sortedBy { verkehrOrder[it.verkehr] }
            .forEach {
                println("%10s %-8s %s".format(it.verkehr, it.ds100, it.name))
            }

        println()
        println("Match rates for EVA by Verkehrsart")
        println()
        println("Verkehrsart   Num. Stations    Match Rate")
        println("-----------   -------------    ----------")
        val unmatchedCounts = evaByName.values.groupingBy { it.verkehr }.eachCount()
        val matchedCounts = stationsByEvaId.values.flatMap { it.evaStations }.groupingBy { it.verkehr }.eachCount()
        unmatchedCounts.forEach { verkehr, unmatched ->
            val matched = matchedCounts[verkehr] ?: 0
            val total = matched + unmatched
            val rate = matched * 100 / total
            println("%10s    %-10d       %d".format(verkehr, total, rate))
        }
    }

    fun printMergedUnmatched() {
        // -- show all of them sorted together to easily see similar names
        data class Unmatched(val eva: Char, val name: String, val ds100: String)

        stationsByBfNr.values.toSet()
            .filter { it.evaStations.isEmpty() }
            .map { Unmatched(' ', it.name, it.ds100) }
            .union(
                evaByName.values.map {
                    Unmatched('E', it.name, it.ds100.joinToString("/"))
                }
            )
            .sortedBy { it.name }
            .forEach {
                println("%s %-15s %s".format(it.eva, it.ds100, it.name))
            }
    }

    fun printMultistations() {
        val numParts = multistations.map { it.evaStations.size }.sum()
        println()
        println("We have ${multistations.size} multi-stations with ${numParts} EVA parts and here they are: ")
        multistations
            .sortedBy { it.ds100.first() + it.name }
            .forEach { infraStation ->
                println("${infraStation.name} – ${infraStation.ds100}")
                infraStation.evaStations.forEach { evaStation ->
                    println("    ${evaStation.name} – ${evaStation.evaId} ${evaStation.ds100}")
                }
            }
    }

    fun writeStationsJson(fileName: String, stations: List<InfraStation>) {
        /*
        Json data in Dart is Map<dynamic>, List<dynamic>, and the three primitives.
        Conversion is manual by traversing the tree.
        It seems like a good idea to deliver our data package as an object (map) rather than an array,
        because the frontend can then make direct lookups and convert only the parts that it needs to
        proper Dart objects!!

        Also, the data for station-selection only has [longname] so that exactly has to be the key in
        our maps!
         */
        val stationsMap = stations.map { it.longestName to it }.toMap()
        val numLost = stations.size - stationsMap.size
        if (numLost > 0) {
            logger.warn("Ambiguous station names: lost $numLost entries!")
        }
        val evaIdToStationName = stationsByEvaId.mapValues { it.value.longestName }

        val objectMapper = jacksonObjectMapper()
        objectMapper.writeValue(File(fileName),
            StationsWrapper(stationsMap, evaIdToStationName, getMultistationParts()))
    }

    fun writeAllStationsJson(fileName: String) {
        writeStationsJson(fileName, stationsByName.values.toList())
    }

    sealed class PlatformFilter() {
        class By(val predicate: (Platform) -> Boolean) : PlatformFilter()
        object Rest : PlatformFilter()
    }

    internal fun getMultistationParts(): Map<String, List<Platform>> {
        return multistations.flatMap { infra ->
            val platforms = mutableListOf<Platform>().apply { addAll(infra.platforms) }
            val results = mutableListOf<Pair<String, List<Platform>>>()
            val restParts = mutableListOf<EvaStation>()
            infra.evaStations.forEach { eva ->
                val filter = getMultistationPlatformFilter(eva)
                if (filter is PlatformFilter.Rest) {
                    restParts.add(eva)
                } else if (filter is PlatformFilter.By) {
                    results.add(eva.evaId to platforms.extract(filter.predicate))
                }
            }
            if (restParts.size > 1) {
                val evaNames = restParts.map { it.name }
                logger.warn("Several parts of ${infra.name} get assigned all remaining tracks: $evaNames")
            }
            restParts.forEach { results.add(it.evaId to platforms.toList()) }
            results
        }.toMap()
    }

    internal fun getMultistationPlatformFilter(evaStation: EvaStation): PlatformFilter {
        val sBahnSuffixes = listOf("(S)", "(S-Bahn)", "(tief)")
        return when {
            evaStation.ds100.first() == "BL" -> // Berlin Hbf (tief)
                PlatformFilter.By { it.platformName.matches(Regex("B0[1-4]")) }

            evaStation.ds100.first() == "LL  T" -> // Leipzig Hbf (tief)
                PlatformFilter.By { it.platformName == "B01" }

            evaStation.ds100.first() == "NND" -> // Nürnberg Frankenstadion Sonderbahnsteig
                PlatformFilter.By { false }

            sBahnSuffixes.any { evaStation.name.endsWith(it) } ->
                PlatformFilter.By { it.platformName.startsWith("SB") }

            evaStation.name.contains("Gl.") ->
                PlatformFilter.By { it.trackNumber in getTracks(evaStation.name) }

            else -> PlatformFilter.Rest
        }
    }

    private fun getTracks(stationName: String): IntRange {
        val regex = Regex("""Gl[.] ?(\d+)(-(\d+))?""")
        val match = regex.find(stationName)!!
        val first = match.groupValues[1].toInt()
        val last = if (match.groupValues[3].isNotEmpty()) {
            match.groupValues[3].toInt()
        } else
            first
        return first..last
    }

    fun printLongestNamesDifferentFromInfra() {
        stationsByName.values.forEach {
            val long = it.longestName
            if (long != it.name) {
                println("${it.name}  -->  ${long}")
            }
        }
    }

    fun printEvaNamesShorterThanInfra() {
        stationsByName.values.forEach {
            val evaName = it.evaStations.minBy { it.name.length }.name
            if (evaName.length < it.name.length) {
                println("${it.name}  -->  ${evaName}")
            }
        }
    }

    /*
    Writes all station names in order of number of platforms descending.
    We use the name from infra-list and additionally add the unmatched EVA names
    at the end of the list (as if they had zero platforms).
     */
    fun writeStationNamesForAutoComplete(fileName: String) {
        // show biggest stations first for good user-experience in frontend
        val infraStations = stationsByName.values
            .sortedByDescending { it.platforms.size }
            .map { it.longestName }

        // also show stations without infra-data, but put them last.
        val evaStations = evaByName.map { it.key }

        val stations = infraStations.plus(evaStations)
        val objectMapper = jacksonObjectMapper()
        objectMapper.writeValue(File(fileName), StationNamesWrapper(stations))
    }

    /* statistics
        - largest stations by number of platforms
        - largest stations by added length of platforms
        - number of stations for each number of platforms
        - distribution of platform heights vs platform lengths
     */
    fun largestStations(num: Int) {
        val allStations = stationsByName.values
        val stationsByNumberOfP = allStations.sortedByDescending { it.platforms.size }.take(num)
        println("")
        println("${num} largest stations by number of platforms:")
        stationsByNumberOfP.forEach {
            println("% 2d".format(it.platforms.size) + " ${it.name}")
        }
        val stationsByLengthOfP = allStations.sortedByDescending { it.totalPlatformLength }.take(num)
        println("")
        println("${num} largest stations by cumulative length of platforms:")
        stationsByLengthOfP.forEach {
            println("% 6d".format(it.totalPlatformLength) + " ${it.name}")
        }
    }

    fun numberOfStationsBySize() {
        val counts = stationsByName.values.groupingBy { it.platforms.size }.eachCount()
        println()
        println("#platforms  #stations with that many platforms")
        counts.keys.sortedDescending().forEach {
            println(" % 3d".format(it) + "      " + "% 5d".format(counts[it]))
        }
        println("There are ${stationsByName.size} stations in total.")
        // 11 stations with more than 13 platforms and
        // 29 stations with more than 10 platforms (including the above)
        // and then more than three quarters, almost 80% of all stations have only one platform or two.
    }

    fun numberOfStationsByPlatformHeight() {
        val counts = stationsByName.values.groupingBy { it.platformHeightsSummary }.eachCount()
        println()
        println("#platformHeights  #stations with those heights")
        counts.keys.sortedDescending().forEach {
            println(" %10s".format(it) + "      " + "% 5d".format(counts[it]))
        }
        println("There are ${stationsByName.size} stations in total.")
    }

    fun numberOfPlatformsByHeight() {
        val platforms = stationsByName.values.flatMap { it.platforms }
        val counts = platforms.groupingBy {
            if (it.height in 34..38) "34 .. 38"
            else if (it.height in 20..33) "20 .. 32"
            else if (it.height < 20) "< 20"
            else it.height.toString()
        }.eachCount()
        println()
        println("#height  #platforms with this height")
        // Doing that string manipulation inside the sort seems inefficient, but since there are only 8 keys here
        // after grouping, it takes practically no time and so I prefer the shorter source code.
        counts.keys.sortedBy {
            if (it.first() == '<') 0 else it.takeWhile { c -> c.isDigit() }.toInt()
        }.forEach {
            println("%9s".format(it) + "      " + "% 5d".format(counts[it]))
        }
        println("There are ${platforms.size} platforms in total.")
    }

    fun longestAndShortestPlatforms() {
        val platforms = stationsByBfNr.values
            .flatMap { station ->
                station.platforms.map {
                    Pair("${station.longestName} ${it.trackName}", it.length)
                }
            }.sortedBy { it.second }

        println()
        println("Longest Platforms: ")
        platforms.asReversed().takeWhile { it.second > 500 }
            .forEach {
                println("%4d %s".format(it.second, it.first))
            }

        println()
        println("Shortest Platforms: ")
        platforms.take(20)
            .forEach {
                println("%4d %s".format(it.second, it.first))
            }
    }
}

fun lotsOfStatistics(stations: DbStations) {
    println()
    println("------------------------------------")
    println()
    stations.printMultistations()
    stations.longestAndShortestPlatforms()
    stations.largestStations(20)
    stations.numberOfStationsBySize()
    stations.numberOfStationsByPlatformHeight()
    stations.numberOfPlatformsByHeight()
}

fun lotsOfDebugInfo(stations: DbStations) {
    stations.printUnmatched()
    // stations.printMergedUnmatched()
    // stations.printLongestNamesDifferentFromInfra()
    // stations.printEvaNamesShorterThanInfra()
}

fun main(args: Array<String>) {
    val stations = DbStations()
    stations.readAllTheThings()
//    stations.lotsOfStatistics(stations)
//    stations.lotsOfDebugInfo(stations)
//    return

    stations.writeStationNamesForAutoComplete(FRONTEND_STATIC_DATA_DIR + "stationNames.json")
    stations.writeAllStationsJson(FRONTEND_STATIC_DATA_DIR + "allStations.json")
}
