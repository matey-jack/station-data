package util

/**
 * Returns a list containing all elements from the receiver for which 'predicate' is true,
 * and also removes those elements from the receiver. Preserves the order of elements in both lists.
 *
 * This does conceptually the same thing as Iterable.partition(), but with mutating the argument
 * in place to give the first result. This fits our use-case better than having to deal with
 * a pair.
 */
fun <E> MutableList<E>.extract(filter: (E) -> Boolean): List<E> {
    val result = mutableListOf<E>()
    var i = 0
    var j = 0
    // i is the index after the last element that we have checked
    // j is the index after the last element that is still in the list
    while (i < size) {
        if (filter(get(i))) {
            result.add(get(i))
        } else {
            set(j, get(i))
            j++
        }
        i++
    }
    // Java's funny way of truncating a list. Top-rated on StackOverflow!
    subList(j, size).clear()
    return result
}

// TODO: documentation && unit test !! TODO
// purely functional version is highly interesting although not better:
// https://repl.it/@matey_jack/FlipMap
fun <K1, K2, V>flipKeys(input: Map<K1, Map<K2, V>>): Map<K2, HashMap<K1, V>> {
    val result = HashMap<K2, HashMap<K1, V>>()
    input.forEach { k1, innerMap ->
        innerMap.forEach { k2, v ->
            result.getOrPut(k2) { HashMap() }[k1] = v
        }
    }
    return result
}