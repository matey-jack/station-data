plugins {
    kotlin("jvm") version "1.9.23"
    java
    id("com.github.ben-manes.versions") version "0.20.0"
}

group = "matey-jack"
version = "1.0-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.toVersion("17")
}

kotlin {
    jvmToolchain(17)
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.apache.commons:commons-csv:1.6")

    // slf4j comes indirectly with kotlin-logging
    implementation("io.github.microutils:kotlin-logging:1.6.20")
    implementation("ch.qos.logback:logback-classic:1.2.3")

    implementation("com.fasterxml.jackson:jackson-base:2.9.7")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.7")
    // pull indirect dependency up to current kotlin version:
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.3.10")

    testImplementation(platform("org.junit:junit-bom:5.10.3"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
    testImplementation("org.assertj:assertj-core:3.11.1")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}
