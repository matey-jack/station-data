Folgendes lässt sich aus den Ergebnisse der Methode regionStats() in DbStations.kt ablesen.

Aufgabenträger gehören immer zu einem Bundesland, wobei 6 von 16 Ländern ihr Gebiet nochmal aufteilen und die anderen nicht.

Ungeteilte Länder sind:
 - Bayern
 - Berlin
 - Brandenburg
 - Bremen
 - Hamburg
 - Mecklenburg
 - Saarland
 - Sachsen-Anhalt
 - Schleswig-Holstein
 - Thüringen
9 Aufgabenträger, da Berlin und Brandenburg gemeinsam bestellen.

Aufteilung nach Fläche:
 - Rheinland-Pfalz (Nord + Süd)
 - NRW (Rheinland + Rhein-Ruhr + Westfalen-Lippe)
 - Sachsen (Leipzig + Mittelsachsen + Vogtland + Oberelbe + Lausitz)
10 Aufgabenträger

Aufteilung nach Metropolregion + Rest:
 - Baden-Württemberg (Stuttgart + Rest)
 - Hessen (Rhein-Main + Rhein-Neckar + Nordhessen, aka der Rest)
 - Niedersachsen (Hannover + Braunschweig + Rest)
8 Aufgabenträger

also insgesamt 27 Aufgabenträger.

Übrigens gibt es noch zwei spannende Ausnahmen:
 - Bf. Tromsdorf (RB Südost, Erfurt) an der thüringischen Pfefferminzbahn
      liegt in Sachsen-Anhalt, aber wird von den Thüringen beauftragt
 - Bf. Staufenberg-Speele (RB Mitte, Kassel) liegt in Niedersachsen, aber wird von den Nordhessen beauftragt.
      Vermutlich ist das so, weil Staufenberg-Speele zum Einzugsbereich der Großstadt Kassel gehört


Verkehrsverbünde gibt es übrigens deutlich mehr! Diese unterscheiden sich aber stark:
 - manche koordinieren nur Fahrpläne, aber keine Tarife
 - in anderen Fällen ist ein großer Verbund für die Bahn zuständig und viele kleine für die Busse
 - 9 von 16 Ländern (also 6 Flächenländer) haben Verbund-Tarife für das ganze Land:
   - die Stadtstaaten sowie Saarland und Brandenburg genau einen Verbund im Land
     (wobei die Verbünde von Hamburg und Bremen in die Nachbarländer reichen, auch wenn Auftraggeber anders ist)
   - NRW, RP und Hessen mehrere Verbünde, die alles abdecken
   - die anderen 7 Länder sind ziemlich durcheinander
 - für die Bahn gibt es ja noch den traditionellen Kilometertarif, der angewendet wird, wenn es keinen Verbund-Tarif gibt
   wann jetzt der und nicht ein anderer anzuwenden ist, kann wohl aber ganz schön schwierig sein, besonders wenn mensch
   über eine Grenze fährt.


Bahn-seitig gibt es statt der alten 15 Bahndirektionen jetzt sieben Regionen mit insgesamt 47 Unterzentren.
Diese Regionen decken sich ganz gut mit den Bundesländern, wobei in sechs Fällen einzelne Grenz-Bahnhöfe dem
Nachbarbereich zugeordnet sind (wahrscheinlich technische Gründe mit Stellwerk und so). Außerdem sind noch
 - 10 hessische Bahnhöfe in der Region Südwest
 - 5 hessische Bahnhöfe in der Region Nord
 - 3 bayrische Bahnhöfe in Südwest
Da die drei größten Bundesländer haben ihren eigenen Regionalbereich und die anderen sind zusammengefasst.
Genau wie bei den Rundfunkanstalten, nur etwas logischer ;-)

Lage der Regionalbereiche:
Nord --> Nord-West Deutschland
Ost --> Nord-Ost Deutschland
West --> West-Nord-West, NRW eben
Mitte --> Mitte-West
Südost --> Mitte-Ost
Südwest --> Süd-West
Süd --> Süd-Ost
Kurz gesagt, es ist aufgeteilt entlang einer Nord-Süd-Achse in Lage der ehem. innerdeutschen Grenze, die nach Süden
verlängert wird und dort zwischen Bayern und Bawü verläuft. Östlich davon gibt es von Nord nach Süd drei getrennte
Bereich und westlich gibt es vier.



Die Aufteilung innerhalb der Bahn-Regionen passt allerdings nicht zu den Aufgabenträgern, mit der Ausnahme von
Rheinland-Pfalz, wo die Regierung den AG Nord und AG Süd jeweils genau zwei Regionalzentren der Bahn zugeordnet hat.
(Wobei eines der in RP befindliche Teil der Zentrale Saarbrücken ist.)

