
This project helps you discover data on German train stations, especially the height and length of platforms which matters for many reasons including accessibility, compatibility with different rolling stock (aka trains), and transport capacity.

Play with it on http://station-data.surge.sh/

Parts of the project:
 - [backend]: pre-processes data from Deutsche Bahn's Open Data portal http://data.deutschebahn.com/organization/db-station-service
   you don't need to run this unless you want to add any functionality, since the
   results of processing are all committed in [frontend/web/data]. You can also use that preprocessed data in your own projects.
   
 - [frontend]: HTML and JavaScript to be served from a simple file storage (or a local dev-server, see [frontend/Readme.md]).

